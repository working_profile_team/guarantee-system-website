#coding=UTF-8
from tablib.packages.ordereddict import OrderedDict
from django.conf.urls import patterns

from django.contrib import admin, messages
from django.contrib.admin.util import flatten_fieldsets, unquote
from django.core import urlresolvers, exceptions
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import formats as django_formats
from django_tablib.admin import TablibAdmin
from django.conf import settings
from garantias.models import *
from garantias.forms import *


class ReadonlyStackedInline(admin.StackedInline):

    def get_readonly_fields(self, request, obj=None):
        if self.declared_fieldsets:
            return flatten_fieldsets(self.declared_fieldsets)
        else:
            return list(set(
               [field.name for field in self.opts.local_fields] +
               [field.name for field in self.opts.local_many_to_many]
            ))

    def has_add_permission(self, request):
        False

    def has_delete_permission(self, request, obj=None):
        False


class ParticipanteSolicitudAdmin(admin.ModelAdmin):

    # Filtro el listado para cada grupo de usuarios.
    # Los administrador de solicitudes pueden ver todas.
    # Los vendedores de solicitudes pueden ver sólo los inquilinos de las solicitudes de la oficina a la que pertenecen
    def queryset(self, request):
        qs = super(ParticipanteSolicitudAdmin, self).queryset(request)
        user = GarantiasUser.create_from(request.user)
        if user.es_administrador() or user.es_administrador_de_solicitudes():
            return qs
        elif user.es_vendedor_de_solicitudes():
            return qs.filter(solicitud__oficina=request.user.vendedor.oficina)
        else:
            return qs.none()

    fieldsets = (
        (None, {'fields': ('nombre_completo', 'tipo_doc', 'num_doc',
                           'fecha_nacimiento', 'nacionalidad', 'estado_civil',
                           'domicilio', 'domicilio_ciudad',
                           'domicilio_provincia', 'telefono', 'celular',
                           'email', )}),
        (u'Información laboral', {'fields': ('empleado_tipo', 'cuit', )}),
        (u'Ocupación Actual', {'fields': ('empresa_ocu_actual',
                                          'actividad_ocu_actual',
                                          'domicilio_ocu_actual',
                                          'localidad_ocu_actual',
                                          'telefono_ocu_actual',
                                          'puesto_ocu_actual',
                                          'puesto_seccion_ocu_actual', )}),
        (u'Declaración de ingresos', {'fields': ('ingresos_monto', )}),
    )


class InquilinoAdmin(ParticipanteSolicitudAdmin):
    form = InquilinoAdminForm

    list_display = ('nombre_completo', '_documento', '_domicilio', 'email', 'telefono', 'celular', '_solicitud_nro',
                    '_como_nos_conocio')
    search_fields = ('nombre_completo', 'num_doc', 'solicitud__nro')

    def _documento(self, obj):
        if obj.num_doc:
            return '%s %s' % (obj.tipo_doc, obj.num_doc)
        return u'No asignado'

    _documento.short_description = "Documento"
    _documento.admin_order_field = 'num_doc'

    def _domicilio(self, obj):
        if obj.domicilio:
            return '%s (%s)' % (obj.domicilio, obj.domicilio_ciudad)
        return u'No asignado'

    _domicilio.short_description = "Domicilio Actual"
    _domicilio.admin_order_field = 'domicilio'

    def _solicitud_nro(self, obj):
        if obj.solicitud:
            return obj.solicitud.nro
        return u'No asignado'

    _solicitud_nro.short_description = "Nro Solicitud"
    _solicitud_nro.admin_order_field = 'solicitud__nro'

    def _como_nos_conocio(self, obj):
        try:
            inquilinoEncuesta = obj.encuesta_inquilino
        except InquilinoEncuesta.DoesNotExist:
            return u'Sin información'
        if inquilinoEncuesta:
            return u'%s' % inquilinoEncuesta.get_como_nos_conocio_display()
        else:
            return u'Ninguna'

    _como_nos_conocio.short_description = u"Cómo nos conoció"
    _como_nos_conocio.admin_order_field = 'encuesta_inquilino__como_nos_conocio'


class CosolicitanteAdmin(ParticipanteSolicitudAdmin):
    form = CosolicitanteAdminForm

    list_display = ('nombre_completo', '_documento', '_domicilio', 'email', 'telefono', 'celular', '_solicitud_nro')
    search_fields = ('nombre_completo', 'num_doc', 'solicitud__nro')

    def _documento(self, obj):
        if obj.num_doc:
            return '%s %s' % (obj.tipo_doc, obj.num_doc)
        return u'No asignado'

    _documento.short_description = "Documento"
    _documento.admin_order_field = 'num_doc'

    def _domicilio(self, obj):
        if obj.domicilio:
            return '%s (%s)' % (obj.domicilio, obj.domicilio_ciudad)
        return u'No asignado'

    _domicilio.short_description = "Domicilio Actual"
    _domicilio.admin_order_field = 'domicilio'

    def _solicitud_nro(self, obj):
        if obj.solicitud:
            return obj.solicitud.nro
        return u'No asignado'

    _solicitud_nro.short_description = "Nro Solicitud"
    _solicitud_nro.admin_order_field = 'solicitud__nro'


class AvisoIncumplimientoAdmin(admin.ModelAdmin):
    model = AvisoIncumplimiento
    list_display = ('fecha', 'nro_cert_garantia', )

    # Filtro el listado para cada grupo de usuarios.
    # Los administrador de solicitudes pueden ver todas.
    # Los vendedores de solicitudes pueden ver sólo los avisos de incumplimiento de las solicitudes de la oficina
    # a la que pertenecen
    def queryset(self, request):
        qs = super(AvisoIncumplimientoAdmin, self).queryset(request)
        user = GarantiasUser.create_from(request.user)
        if user.es_administrador() or user.es_administrador_de_solicitudes():
            return qs
        elif user.es_vendedor_de_solicitudes():
            return qs.filter(solicitud__oficina=request.user.vendedor.oficina)
        else:
            return qs.none()

    def get_readonly_fields(self, request, obj=None):
        if obj:  # Editing
            return ('fecha', )
        return ()


class ComprobanteAdmin(admin.ModelAdmin):
    model = Comprobante
    list_display = ('solicitud', 'descripcion',)

    def get_readonly_fields(self, request, obj=None):
        if obj:  # Editing
            return ('fecha', )
        return ()


#Begin SolicitudGarantiaAdmin inlines#
#Editable inlines
class ParticipanteSolicitudInline(admin.StackedInline):
    fieldsets = ParticipanteSolicitudAdmin.fieldsets
    extra = 0


class InquilinoInline(ParticipanteSolicitudInline):
    form = InquilinoAdmin.form
    model = Inquilino
    verbose_name_plural = u'inquilino'


class CosolicitanteInline(ParticipanteSolicitudInline):
    form = CosolicitanteAdmin.form
    model = Cosolicitante


class AvisoIncumplimientoInline(admin.StackedInline):
    form = AvisoIncumplimientoAdminInlineForm
    model = AvisoIncumplimiento
    extra = 0


class ComprobanteInline(admin.StackedInline):
    form = ComprobanteAdminInlineForm
    model = Comprobante
    extra = 0

#Readonly inlines
class ParticipanteSolicitudInlineReadonly(ReadonlyStackedInline):
    fieldsets = ParticipanteSolicitudAdmin.fieldsets
    extra = 0

class InquilinoInlineReadonly(ParticipanteSolicitudInlineReadonly):
    form = InquilinoAdmin.form
    model = Inquilino
    verbose_name_plural = u'inquilino'


class CosolicitanteInlineReadonly(ParticipanteSolicitudInlineReadonly):
    form = CosolicitanteAdmin.form
    model = Cosolicitante


class AvisoIncumplimientoInlineReadonly(ReadonlyStackedInline):
    form = AvisoIncumplimientoAdminInlineForm
    model = AvisoIncumplimiento
    extra = 0


class ComprobanteInlineReadonly(ReadonlyStackedInline):
    form = ComprobanteAdminInlineForm
    model = Comprobante
    extra = 0
#End SolicitudGarantiaAdmin inlines#


class SolicitudGarantiaAdmin(TablibAdmin):
    date_hierarchy ='fecha'
    ordering = ('-ultima_modificacion', )
    save_on_top = True
    inlines = [InquilinoInline, CosolicitanteInline, AvisoIncumplimientoInline, ComprobanteInline,
               InquilinoInlineReadonly, CosolicitanteInlineReadonly, AvisoIncumplimientoInlineReadonly,
               ComprobanteInlineReadonly]

    def get_inline_instances(self, request, obj=None):
        inline_instances = super(SolicitudGarantiaAdmin, self).get_inline_instances(request, obj)
        user = GarantiasUser.create_from(request.user)
        #filtre a los inlines por el nombres de la clase porque no siempre el padre me da en el mismo orden los
        # elementos, aparte en caso de refactorizar la clase, se hace mas sencillo
        if obj and not user.es_administrador_de_solicitudes() and \
                obj.estado in ('ARESOLUCION', 'PREAPROBADA', 'APROBADA', 'RECHAZADA') and not user.es_administrador():
            new_inline_instances = [inline for inline in inline_instances if inline.__class__ in (
            InquilinoInlineReadonly, CosolicitanteInlineReadonly, AvisoIncumplimientoInlineReadonly,
            ComprobanteInlineReadonly)]
        else:
            new_inline_instances = [inline for inline in inline_instances if inline.__class__ in (
            InquilinoInline, CosolicitanteInline, AvisoIncumplimientoInline, ComprobanteInline)]
        return new_inline_instances

    #Columnas del changelist
    def _fecha_de_alta(self, obj):
        return obj.get_fecha_de_alta()
    _fecha_de_alta.short_description = "Fecha de Alta"
    _fecha_de_alta.admin_order_field = 'fecha'

    def inquilino_nombre_completo(self, obj):
        return obj.get_inquilino_nombre_completo()
    inquilino_nombre_completo.short_description = "Apellido y nombre"
    inquilino_nombre_completo.admin_order_field = 'inquilino__nombre_completo'

    def inquilino_num_doc(self, obj):
        return obj.get_inquilino_num_doc()
    inquilino_num_doc.short_description = "Nro. doc."
    inquilino_num_doc.admin_order_field = 'inquilino__num_doc'

    def _inquilino_telefono_y_celular(self, obj):
        return obj.get_inquilino_telefono_y_celular()
    _inquilino_telefono_y_celular.short_description = "Tel. / Cel."
    _inquilino_telefono_y_celular.admin_order_field = 'inquilino__telefono'

    def _inmobiliaria(self, obj):
        return obj.get_inmobiliaria()
    _inmobiliaria.short_description = "Inmobiliaria"
    _inmobiliaria.admin_order_field = 'inmobiliaria__nombre'

    def _provincia(self, obj):
        return obj.get_domicilio_provincia()
    _provincia.short_description = "Provincia"
    _provincia.admin_order_field = 'inquilino__domicilio_provincia'

    def _ciudad(self, obj):
        return obj.get_domicilio_ciudad()
    _ciudad.short_description = "Ciudad"
    _ciudad.admin_order_field = 'inquilino__domicilio_ciudad'

    def _ultima_modificacion(self, obj):
        return obj.get_ultima_modificacion()
    _ultima_modificacion.short_description = "últ. modif."
    _ultima_modificacion.admin_order_field = 'ultima_modificacion'

    def print_button(self, obj):
        link = '<a href="%s/imprimir" target="_blank"><img src="%simages/printer.png">Imprimir</img></a>' % (
            obj.id, settings.STATIC_URL)
        return link
    print_button.short_description = "Imprimir"
    print_button.allow_tags = True

    def _vendedor(self, obj):
        return obj.get_vendedor()
    _vendedor.short_description = "Vendedor"
    _vendedor.admin_order_field = 'vendedor__username'

    def _oficina(self, obj):
        return obj.get_oficina()
    _oficina.short_description = "Oficina"
    _oficina.admin_order_field = 'oficina__nombre'

    list_display = ('nro', '_fecha_de_alta',
                    'inquilino_nombre_completo', 'inquilino_num_doc',
                    '_inquilino_telefono_y_celular',
                    '_ciudad', '_provincia', '_ultima_modificacion',
                    '_inmobiliaria', '_vendedor', '_oficina', 'estado', 'print_button',
    )
    list_display_links = ('nro', )
    list_filter = ('estado', 'fecha', 'vendedor', 'oficina')
    search_fields = (
        'inquilino__num_doc', 'inquilino__nombre_completo', '=nro',
        'inmobiliaria__nombre', 'inquilino__domicilio_ciudad')

    # Opciones de exportación
    formats = ['xls', ]
    headers = OrderedDict([('Nro.', 'nro'), ('Fecha de Alta', 'get_fecha_de_alta'),
                           ('Nombre completo del inquilino', 'get_inquilino_nombre_completo'),
                           ('Nro. Doc. del inquilino', 'get_inquilino_num_doc'),
                           ('Tel. / Cel.', 'get_inquilino_telefono_y_celular'),
                           ('Ciudad.', 'get_domicilio_ciudad'),
                           ('Provincia', 'get_domicilio_provincia'),
                           ('Última modificación', 'get_ultima_modificacion'),
                           ('Inmobiliaria', 'get_inmobiliaria'),
                           ('Vendedor', 'get_vendedor'),
                           ('Oficina', 'get_oficina'),
                           ('Estado', 'estado'),
                           ('Domicilio', 'get_domicilio')
    ])
    export_filename = 'listado_solicitudes_%s' % django_formats.date_format(datetime.date.today(), "SHORT_DATE_FORMAT")

    # Filtro el listado para cada grupo de usuarios.
    # Los administrador de solicitudes pueden ver todas.
    # Los vendedores de solicitudes pueden ver sólo las solicitudes de la oficina a la que pertenecen.
    def queryset(self, request):
        qs = super(SolicitudGarantiaAdmin, self).queryset(request)
        user = GarantiasUser.create_from(request.user)
        if user.es_administrador() or user.es_administrador_de_solicitudes():
            return qs
        elif user.es_vendedor_de_solicitudes():
            return qs.filter(oficina=request.user.vendedor.oficina)
        else:
            return qs.none()

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == "vendedor":
            user = GarantiasUser.create_from(request.user)
            usuarios_vendedores = User.objects.filter(groups__name='Vendedor de Solicitudes')  # FIXME: Sin hardcodear!
            # Si soy administrador de solicitudes, puedo seleccionar cualquier vendedor
            # Si soy sólo vendedor de solicitudes, puedo reasignar sólo a las personas de mi oficina
            if user.es_administrador() or user.es_administrador_de_solicitudes():
                kwargs["queryset"] = usuarios_vendedores.all()
            elif user.es_vendedor_de_solicitudes():
                kwargs["queryset"] = usuarios_vendedores.filter(vendedor__oficina=user.vendedor.oficina)
        return super(SolicitudGarantiaAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    # Permito agregar solicitudes sólo a los administradores y vendedores de solicitudes
    # Notar que no alcanza con los permisos de grupo porque el caso de un usuario miembro de 'Vendedor de Solicitudes'
    # pero sin una oficina asignada
    def has_add_permission(self, request):
        user = GarantiasUser.create_from(request.user)
        return user.es_administrador() or user.es_administrador_de_solicitudes() or user.es_vendedor_de_solicitudes()

    # Permito a cada grupo de usuarios modificar sólo las solicitudes que les
    # corresponden (misma condición que el queryset)
    def has_change_permission(self, request, obj=None):
        user = GarantiasUser.create_from(request.user)
        if obj and obj.oficina:
            return user.es_administrador() or user.es_administrador_de_solicitudes() or \
                user.es_vendedor_de_solicitudes_en_oficina(obj.oficina)
        else:
            return user.es_administrador() or user.es_administrador_de_solicitudes() or \
                user.es_vendedor_de_solicitudes()

    # Permito a cada grupo de usuarios eliminar sólo las solicitudes que deberían poder eliminar
    def has_delete_permission(self, request, obj=None):
        user = GarantiasUser.create_from(request.user)
        if obj:  # specific object
            if user.es_administrador() or user.es_administrador_de_solicitudes():
                return True
            elif user.es_vendedor_de_solicitudes() and user.es_vendedor_de_solicitudes_en_oficina(obj.oficina):
                # Notar que un vendedor de solicitudes puede borrar cualquier solicitud de la oficina,
                # aunque no sea suya.
                return obj.estado not in ('ARESOLUCION', 'PREAPROBADA', 'APROBADA', 'RECHAZADA')
        else:  # any object
            return user.es_administrador() or user.es_administrador_de_solicitudes()

    # Defino los campos a MOSTRAR en change_view
    def get_fieldsets(self, request, obj=None):
        # Fieldsets comunes a todos
        self.fieldsets = (
            (u'Sobre la solicitud', {'fields': ['inmobiliaria', 'estado', 'observaciones', ]}),
            (u'Sobre el contrato', {'fields': ['alquiler_pactado',
                                               'alquiler_pactado_segundo_anio',
                                               'alquiler_pactado_tercer_anio',
                                               'expensas_pactadas',
                                               'contrato_meses', ]}),
        )

        # Agrego campos extra de asignación para los administradores
        user = GarantiasUser.create_from(request.user)
        if user.es_administrador() or user.es_administrador_de_solicitudes():
            self.fieldsets[0][1]['fields'] = ['vendedor', 'oficina', ] + self.fieldsets[0][1]['fields']
        elif user.es_vendedor_de_solicitudes():
            # Para vendedores de solicitudes, muestro el campo vendedor en caso de que esté editando,
            # para que puedan asignarse (o reasignar a otros) las solicitudes dentro de la oficina.
            # Notar que cualquier vendedor de solicitudes puede reasignar cualquier solicitud de la oficina,
            # aunque no le pertenezca.
            if obj:
                self.fieldsets[0][1]['fields'] = ['vendedor'] + self.fieldsets[0][1]['fields']

        # Si estoy editando, agrego los campos solo-lectura
        # NOTA: Esta parte se comento por ahora porque generaba error al editar y luego intentar agregar
        #if obj:
        #    readonly_fields = list(self.get_readonly_fields(request, obj))
        #    self.fieldsets[0][1]['fields'] = readonly_fields + self.fieldsets[0][1]['fields']
        return self.fieldsets

    def change_view(self, request, object_id, form_url='', extra_context=None):
        # FIXME
        # Esto es necesario para arreglar un supuesto bug de Django. Al overridear get_fieldsets() para agregar unos
        # fields es necesario fijar self.declared_fieldsets con dichos fields.
        if self.declared_fieldsets:
            obj = self.get_object(request, unquote(object_id))
            # Usamos [0][1]['fields'] porque:
            # 1. declared_fieldsets es una property y no puede ser redefinida.
            # 2. [0] y [1] referencia a tuplas y tampoco pueden ser redefinidas.
            self.declared_fieldsets[0][1]['fields'] = self.get_fieldsets(request, obj)[0][1]['fields']
            #user = GarantiasUser.create_from(request.user)
            #if user.es_administrador() or user.es_administrador_de_solicitudes():
            #    fields = self.declared_fieldsets[0][1]['fields']
            #    if not fields.__contains__('inmobiliaria'):
            #        self.declared_fieldsets[0][1]['fields'] = ['inmobiliaria', 'oficina'] + fields
        return super(SolicitudGarantiaAdmin, self).change_view(request, object_id, form_url, extra_context)

    # Este método era utilizado dentro del método get_fieldsets, para mostrar unos campos en solo
    # lectura. A futuro se piensa volver a agregar en el método pero previamente hay que corregir el bug mencionado
    # arriba.
    def get_readonly_fields(self, request, obj=None):
        user = GarantiasUser.create_from(request.user)
        fields = ()
        # Si estoy editando, fecha y nro son solo-lectura
        if obj:
            fields = ('fecha', 'nro')
            no_es_administrador = not user.es_administrador() and not user.es_administrador_de_solicitudes()
            # Si no es administrador de solicitudes y esta en estado ARESOLUCION, APROBADA o RECHAZADA,
            # todos los campos son solo-lectura
            if no_es_administrador and obj.estado in ('ARESOLUCION', 'PREAPROBADA', 'APROBADA', 'RECHAZADA'):
                if self.declared_fieldsets:
                    return flatten_fieldsets(self.declared_fieldsets)
                else:
                    return list(set(
                        [field.name for field in self.opts.local_fields] +
                        [field.name for field in self.opts.local_many_to_many]
                    ))
        return fields

    def get_urls(self):
        urls = super(SolicitudGarantiaAdmin, self).get_urls()
        my_urls = patterns('',
                           (r'^(.+)/imprimir/$', self.admin_site.admin_view(self.print_solicitud))
        )
        return my_urls + urls

    def print_solicitud(self, request, obj_id):
        solicitud = SolicitudGarantia.objects.get(pk=obj_id)

        #create template page and extend admin/base.html
        print_solicitud_view_template = 'admin/garantias/solicitudgarantia/print_view.html'
        try:
            inquilino = solicitud.inquilino
            provincia = filter(lambda province: province[0] == inquilino.domicilio_provincia,
                               ARProvinceSelect().choices)
            if provincia:
                inquilino.domicilio_provincia = provincia[0][1]
        except Inquilino.DoesNotExist:
            inquilino = None
        cosolicitantes = solicitud.cosolicitantes.all()
        for cosolicitante in cosolicitantes:
            provincia = filter(lambda province: province[0] == cosolicitante.domicilio_provincia,
                               ARProvinceSelect().choices)
            if provincia:
                cosolicitante.domicilio_provincia = provincia[0][1]
        cxt = {
            'solicitud': solicitud,
            'inquilino': inquilino,
            'cosolicitantes': cosolicitantes,
        }
        return render_to_response(print_solicitud_view_template, cxt,
                                  context_instance=RequestContext(request))

    def save_model(self, request, obj, form, change):
        user = GarantiasUser.create_from(request.user)

        # Verifico permisos para aprobar/desaprobar solicitud
        if not self.tiene_permisos_para_aprobacion(user, obj, is_change=change):
            messages.error(request, u'No tiene permisos para realizar esta operación. '
                                    u'Solo los miembros del grupo "Aprueba Solicitud de Garantía" pueden '
                                    u'aprobar una solicitud de garantía '
                                    u'(o desaprobar una solicitud actualmente aprobada).')
            return False

        # Al agregar, si la carga la hace un vendedor de solicitudes,
        # le asigno automáticamente la solicitud y su oficina
        if not change and user.es_vendedor_de_solicitudes():
            if not obj.vendedor:
                obj.vendedor = user
            if not obj.oficina:
                obj.oficina = user.vendedor.oficina

        # Guardo la solicitud
        obj.save()

        # Si la solicitud está aprobada y no tenía una operación asignada, creo una y se la asigno.
        # Si no está aprobada y hay una operación, la elimino.
        operacion = Operacion.objects.filter(solicitud=obj)
        if obj.estado == 'APROBADA':
            if not operacion:
                operacion = Operacion(solicitud=obj)
                operacion.save()
        else:
            if operacion:
                operacion.delete()

    def tiene_permisos_para_aprobacion(self, user, solicitud, is_change):
        # Verifico permisos para aprobar/desaprobar solicitud
        if not is_change:  # On add
            if not user.es_aprueba_solicitud_de_garantia() and solicitud.estado == 'APROBADA':
                return False
        else:  # On Change
            if not user.es_aprueba_solicitud_de_garantia():
                solicitud_old = SolicitudGarantia.objects.get(id=solicitud.pk)
                cambio_a_aprobada = solicitud_old.estado != 'APROBADA' and solicitud.estado == 'APROBADA'
                cambio_a_desprobada = solicitud_old.estado == 'APROBADA' and solicitud.estado != 'APROBADA'
                if cambio_a_aprobada or cambio_a_desprobada:
                    return False
        return True

    def response_change(self, request, obj):
        # HACK para que funcione el botón 'Imprimir' dentro de la solicitud.
        if "_print" in request.POST:
            return HttpResponseRedirect("imprimir")

        # Evito redirigir al listado si no se cuenta con los permisos necesarios.
        user = GarantiasUser.create_from(request.user)
        if not self.tiene_permisos_para_aprobacion(user, obj, is_change=True):
            return HttpResponseRedirect("")

        return super(SolicitudGarantiaAdmin, self).response_change(request, obj)

    def response_add(self, request, obj, post_url_continue='../%s/'):
        # Evito redirigir al listado si no se cuenta con los permisos necesarios.
        user = GarantiasUser.create_from(request.user)
        if not self.tiene_permisos_para_aprobacion(user, obj, is_change=False):
            return HttpResponseRedirect("")

        return super(SolicitudGarantiaAdmin, self).response_change(request, obj)


class EmpleadoDeInmobiliariaInline(admin.TabularInline):
    model = EmpleadoDeInmobiliaria
    extra = 0


class SolicitudGarantiaInline(admin.TabularInline):
    readonly_fields = ('nro', 'fecha', 'inquilino_nombre_completo', 'inquilino_num_doc',
                    '_vendedor', 'estado', '_edit_button')
    form = SolicitudGarantiaInlineForm
    model = SolicitudGarantia
    verbose_name_plural = u'Solicitud de garantia'
    extra = 0

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    #Columnas del changelist
    def _vendedor(self, obj):
        try:
            vendedor = obj.vendedor
        except ObjectDoesNotExist:
            return u'Sin Vendedor'
        if vendedor:
            return u'%s' % vendedor
        else:
            return u'No asignado'

    _vendedor.short_description = "Vendedor"
    _vendedor.admin_order_field = 'vendedor__username'

    def _inmobiliaria(self, obj):
        try:
            inmobiliaria = obj.inmobiliaria
        except Inmobiliaria.DoesNotExist:
            return u'Sin inmobiliaria'
        if inmobiliaria:
            return u'%s' % inmobiliaria
        else:
            return u'Ninguna'

    _inmobiliaria.short_description = "Inmobiliaria"
    _inmobiliaria.admin_order_field = 'inmobiliaria__nombre'

    def inquilino_num_doc(self, obj):
        try:
            inquilino = obj.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if inquilino.num_doc:
            return u'%s' % obj.inquilino.num_doc
        else:
            return u'Sin documento'

    inquilino_num_doc.short_description = "Nro. de documento"
    inquilino_num_doc.admin_order_field = 'inquilino__num_doc'

    def inquilino_nombre_completo(self, obj):
        try:
            inquilino = obj.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if inquilino.nombre_completo:
            return u'%s' % obj.inquilino.nombre_completo
        else:
            return u'Sin nombre'

    inquilino_nombre_completo.short_description = "Apellido y nombre"
    inquilino_nombre_completo.admin_order_field = 'inquilino__nombre_completo'

    def _edit_button(self, obj):
        link = '<a href="%s">%s</a>' % (reverse("admin:garantias_solicitudgarantia_change", args=(obj.id,)), 'Ver / Modificar')
        return link

    #def blog_link(self):
    #  return '<a href="%s">%s</a>' % (reverse("admin:myblog_blog_change",
    #                                    args=(self.blog.id,)), escape(self.blog))

    _edit_button.short_description = "Acciones"
    _edit_button.allow_tags = True


class InmobiliariaAdmin(TablibAdmin):
    model = Inmobiliaria
    inlines = [SolicitudGarantiaInline, EmpleadoDeInmobiliariaInline]

    def get_inline_instances(self, request, obj=None):
        inline_instances = super(InmobiliariaAdmin, self).get_inline_instances(request, obj)
        user = GarantiasUser.create_from(request.user)
        #filtre a los inlines por el nombres de la clase porque no siempre el padre me da en el mismo orden, ni
        #la misma cantidad de inlines
        if user.es_administrador_de_solicitudes() or user.es_administrador():
            new_inline_instances = [inline for inline in inline_instances if inline.__class__ in (
                SolicitudGarantiaInline, EmpleadoDeInmobiliariaInline)]
        else:
            new_inline_instances = [inline for inline in inline_instances if inline.__class__ in (
                EmpleadoDeInmobiliariaInline, )]
        return new_inline_instances

    #IMPORTANTE: En caso de modificar el orden de las columnas, revisar el css y modifcar el min-width para asociarlo a
    # la columna de 'ubicacion'
    list_display = ('id', 'nombre', 'telefonos',
                    'responsable',
                    '_aprobacion_estado', '_fecha_alta',
                    'ubicacion', 'generado_por',
                    '_cantidad_solicitudes', '_cantidad_operaciones',
                    'origen',
                    )
    list_filter = ('aprobacion_estado', 'fecha_alta', 'generado_por', 'origen', )
    search_fields = ('nombre', '=id')

    # Opciones de exportación
    formats = ['xls', ]
    headers = OrderedDict([('Nombre', 'nombre'), ('Domicilio', 'domicilio'), ('Teléfonos', 'telefonos'), ('Fax', 'fax'),
                           ('Email', 'email'), ('Sitio Web', 'sitio_web'), ('Responsable', 'responsable'),
                           ('Estado', 'aprobacion_estado'), ('Fecha de alta', 'fecha_alta'),
                           ('Ubicacion', 'ubicacion'), ('A cargo de', 'generado_por'), ('Origen', 'origen'),
                           ('Cant. sol.', 'get_cantidad_solicitudes'),
                           ('Cant. op.', 'get_cantidad_operaciones')
                           ])
    export_filename = 'listado_inmobiliarias_%s' % django_formats.date_format(datetime.date.today(), "SHORT_DATE_FORMAT")
    date_hierarchy ='fecha_alta'

    def queryset(self, request):
        qs = super(InmobiliariaAdmin, self).queryset(request)
        qs = qs.annotate(cantidad_solicitudes=Count('solicitudes'))
        qs = qs.annotate(cantidad_operaciones=Count('solicitudes__operacion'))
        user = GarantiasUser.create_from(request.user)
        return qs

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == "generado_por":
            user = GarantiasUser.create_from(request.user)
            kwargs["queryset"] = User.objects.filter(groups__name='Promotor de Inmobiliarias')  # FIXME: Sin hardcodear!
        return super(InmobiliariaAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    # Al agregar una inmobiliaria, excluyo el campo 'generado_por' para los promotores
    def get_form(self, request, obj=None, **kwargs):
        if not obj:  # on add
            user = GarantiasUser.create_from(request.user)
            if user.es_promotor_de_inmobiliarias():
                kwargs['exclude'] = ['generado_por', ]
        return super(InmobiliariaAdmin, self).get_form(request, obj, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        user = GarantiasUser.create_from(request.user)
        fields = ()
        if obj:  # Editing
            fields = ('fecha_alta', 'origen', )
            if user.es_promotor_de_inmobiliarias():
                fields += ('generado_por', )
        return fields

    def tiene_permisos_para_aprobacion(self, user, inmobiliaria, is_change):
        # Verifico permisos para aprobar/desaprobar solicitud
        if not is_change:  # On add
            if not user.es_aprueba_inmobiliarias() and inmobiliaria.aprobacion_estado == 'APROBADA':
                return False
        else:  # On Change
            if not user.es_aprueba_inmobiliarias():
                inmobiliaria_old = Inmobiliaria.objects.get(id=inmobiliaria.pk)
                cambio_a_aprobada = inmobiliaria_old.aprobacion_estado != 'APROBADA' and \
                                        inmobiliaria.aprobacion_estado == 'APROBADA'
                cambio_a_desprobada = inmobiliaria_old.aprobacion_estado == 'APROBADA' and \
                                        inmobiliaria.aprobacion_estado != 'APROBADA'
                if cambio_a_aprobada or cambio_a_desprobada:
                    return False
        return True

    #Asigno (al agregar) el  usuario logueado que creo la inmobiliaria
    def save_model(self, request, obj, form, change):

        # Verifico permisos para aprobar/desaprobar inmobiliaria
        user = GarantiasUser.create_from(request.user)
        if not self.tiene_permisos_para_aprobacion(user, obj, is_change=change):
            messages.error(request, u'No tiene permisos para realizar esta operación. '
                                    u'Solo los miembros del grupo "Aprueba inmobiliarias" pueden '
                                    u'aprobar una inmobiliaria '
                                    u'(o desaprobar una inmobiliaria actualmente aprobada).')
            return False

        # Guardo origen y a cargo de quien está la inmobiliaria
        if not change:  # On add
            if user.es_promotor_de_inmobiliarias() and not obj.generado_por:
                obj.generado_por = request.user
            if not obj.origen:
                obj.origen = 'VENDEDOR_FINAER'

        # Guardo la inmobiliaria
        obj.save()

    def response_change(self, request, obj):
        # Evito redirigir al listado si no se cuenta con los permisos necesarios.
        user = GarantiasUser.create_from(request.user)
        if not self.tiene_permisos_para_aprobacion(user, obj, is_change=True):
                return HttpResponseRedirect("")

        return super(InmobiliariaAdmin, self).response_change(request, obj)

    def response_add(self, request, obj, post_url_continue='../%s/'):
        # Evito redirigir al listado si no se cuenta con los permisos necesarios.
        user = GarantiasUser.create_from(request.user)
        if not self.tiene_permisos_para_aprobacion(user, obj, is_change=False):
            return HttpResponseRedirect("")

        return super(InmobiliariaAdmin, self).response_change(request, obj)

    def _cantidad_solicitudes(self, obj):
        return obj.get_cantidad_solicitudes()

    _cantidad_solicitudes.short_description = "Cant. sol."
    _cantidad_solicitudes.admin_order_field = "cantidad_solicitudes"

    def _cantidad_operaciones(self, obj):
        return obj.get_cantidad_operaciones()

    _cantidad_operaciones.short_description = "Cant. op."
    _cantidad_operaciones.admin_order_field = "cantidad_operaciones"

    def _fecha_alta(self, obj):
        return obj.fecha_alta.strftime('%d/%m/%Y')
    _fecha_alta.admin_order_field = 'fecha_alta'

    _fecha_alta.short_description = 'Fecha de alta'

    def _aprobacion_estado(self, obj):
        return obj.get_aprobacion_estado_display()

    _aprobacion_estado.short_description = "Estado"

    # Permito a cada grupo de usuarios modificar sólo las solicitudes que les
    # corresponden
    def has_change_permission(self, request, obj=None):
        user = GarantiasUser.create_from(request.user)
        if obj and obj.generado_por:
            return user.es_administrador() or user.es_administrador_de_inmobiliarias() or \
                user.es_el_promotor(obj.generado_por)
        else:
            return user.es_administrador() or user.es_administrador_de_inmobiliarias() or \
                user.es_promotor_de_inmobiliarias()

    has_delete_permission = has_change_permission

    def change_view(self, request, object_id, form_url='', extra_context=None):
        try:
            return super(InmobiliariaAdmin, self).change_view(request, object_id, form_url, extra_context)
        except exceptions.PermissionDenied:
            messages.error(request, u"Como promotor de inmobiliarias, sólo puede editar las inmobiliarias que "
                                    u"tiene a cargo.")
            return HttpResponseRedirect('/admin/garantias/inmobiliaria/')  # FIXME: Hardcodeo grave!!

    class Media:
        css = {
            'all': ('css/admin/inmobiliaria_admin.css',)
        }


class ConsultaAdmin(admin.ModelAdmin):
    model = Consulta
    list_display = ('nombre_completo', 'fecha')

    def get_readonly_fields(self, request, obj=None):
        if obj:  # Editing
            return ('fecha', )
        return ()


class EmpleadoDeInmobiliariaAdmin(admin.ModelAdmin):
    model = EmpleadoDeInmobiliaria
    list_display = ('apellido', 'nombre', 'email', 'nro_interno', 'inmobiliaria')

    # Filtro el listado para cada grupo de usuarios.
    # Los administrador de inmobiliarias pueden ver todas.
    # Los promotores pueden ver sólo los empleados de las inmobiliarias que tienen a cargo
    def queryset(self, request):
        qs = super(EmpleadoDeInmobiliariaAdmin, self).queryset(request)
        user = GarantiasUser.create_from(request.user)
        if user.es_administrador() or user.es_administrador_de_inmobiliarias():
            return qs
        elif user.es_promotor_de_inmobiliarias():
            return qs.filter(inmobiliaria__generado_por=request.user)
        else:
            return qs.none()

class OficinaForm(forms.ModelForm):
    class Meta:
        model = Oficina

    def clean(self):
        localidades_propias = self.cleaned_data.get('localidades')
        if localidades_propias:
            localidades_asignadas = Localidad.objects.filter(oficinas__isnull=False).exclude(oficinas__pk=self.instance.id)
            for localidad in localidades_propias:
                if localidad in localidades_asignadas:
                    raise forms.ValidationError(
                        'Por lo menos una de las localidades ya fue asignada a otra oficina.')
        return self.cleaned_data


class OficinaAdmin(admin.ModelAdmin):
    form = OficinaForm


class VendedorFinaerAdmin(admin.ModelAdmin):
    list_display = ('usuario', 'oficina', )
    list_filter = ('oficina', )
    model = VendedorFinaer

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == "usuario":
            kwargs["queryset"] = User.objects.filter(groups__name='Vendedor de Solicitudes')  # FIXME: Sin harcodear!
        return super(VendedorFinaerAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class OperacionAdmin(TablibAdmin):
    model = Operacion

    #Columnas del changelist
    def _codigo_solicitud(self, obj):
        if obj.get_solicitud_nro():
            return u'<a href="%s" target="_blank">%s</a>' % (
                urlresolvers.reverse('admin:garantias_solicitudgarantia_change', args=(obj.get_solicitud_id(),)),
                obj.get_solicitud_nro())
        else:
            return u'No asignado'
    _codigo_solicitud.short_description = u'Solicitud'
    _codigo_solicitud.admin_order_field = 'solicitud__nro'
    _codigo_solicitud.allow_tags = True

    def _inquilino_num_doc(self, obj):
        return obj.get_inquilino_num_doc()
    _inquilino_num_doc.short_description = u'Nro. de documento'
    _inquilino_num_doc.admin_order_field = 'solicitud__inquilino__num_doc'

    def _inquilino_nombre_completo(self, obj):
        return obj.get_inquilino_nombre_completo()
    _inquilino_nombre_completo.short_description = u'Apellido y nombre'
    _inquilino_nombre_completo.admin_order_field = 'solicitud__inquilino__nombre_completo'

    def _inquilino_telefono(self, obj):
        return obj.get_inquilino_telefono()
    _inquilino_telefono.short_description = u'Teléfono'
    _inquilino_telefono.admin_order_field = 'solicitud__inquilino__telefono'

    def _inquilino_celular(self, obj):
        return obj.get_inquilino_celular()
    _inquilino_celular.short_description = "Celular"
    _inquilino_celular.admin_order_field = 'solicitud__inquilino__celular'

    def _inquilino_email(self, obj):
        return obj.get_inquilino_email()
    _inquilino_email.short_description = "E-mail"
    _inquilino_email.admin_order_field = 'solicitud__inquilino__email'

    def _fecha_firmada(self, obj):
        return obj.get_fecha_firmada()
    _fecha_firmada.short_description = "Fecha Firmada"
    _fecha_firmada.admin_order_field = 'fecha_firmada'


    list_display = ('codigo', '_codigo_solicitud', '_inquilino_nombre_completo', '_inquilino_num_doc',
                    '_inquilino_telefono', '_inquilino_celular', '_inquilino_email', '_fecha_firmada', 'firmada')
    search_fields = ('=codigo', '=solicitud__nro', '^solicitud__inquilino__num_doc',
                     'solicitud__inquilino__nombre_completo')
    list_filter = ('firmada', )

    readonly_fields = ('codigo', '_codigo_solicitud', '_inquilino_nombre_completo',
                       '_inquilino_num_doc', '_inquilino_telefono', '_inquilino_celular',
                       '_inquilino_email')
    fieldsets = (
        (u'Sobre el Inqulino',
         {'fields': ['_inquilino_nombre_completo', '_inquilino_num_doc',
                     '_inquilino_telefono', '_inquilino_celular', '_inquilino_email', ]}),
        (u'Sobre la Operación',
         {'fields': ['codigo', '_codigo_solicitud', 'anticipo', 'cantidad_cuotas', 'importe_cuota', 'firmada']}),
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(OperacionAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    # Opciones de exportación
    formats = ['xls', ]
    headers = OrderedDict([('Codigo', 'codigo'), ('Codigo de solicitud', 'get_solicitud_nro'),
                           ('Telefono del inquilino', 'get_inquilino_telefono'),
                           ('Celular del inquilino', 'get_inquilino_celular'),
                           ('Email del inquilino', 'get_inquilino_email'),
                           ('Firmada?', 'get_firmada'),
                           ('Fecha Firmada', 'get_fecha_firmada'),
                           ('Nombre completo del inquilino', 'get_inquilino_nombre_completo'),
                           ('Nro Documento del inquilino', 'get_inquilino_num_doc'),
                           ('Hon. en Contrato', 'get_honorario_en_contrato'),
                           ('Inmobiliaria', 'get_inmobiliaria'),
                           ('Honorario', 'get_honorario'),
                           ('Plan', 'get_plan'),
                           ('Anticipo', 'get_anticipo'),
                           ('Importe Cuotas', 'get_importe_cuota'),
    ])
    export_filename = 'listado_operaciones_%s' % django_formats.date_format(datetime.date.today(), "SHORT_DATE_FORMAT")


admin.site.register(SolicitudGarantia, SolicitudGarantiaAdmin)
admin.site.register(Inquilino, InquilinoAdmin)
admin.site.register(Cosolicitante, CosolicitanteAdmin)
admin.site.register(Inmobiliaria, InmobiliariaAdmin)
admin.site.register(Localidad)
admin.site.register(Consulta, ConsultaAdmin)
admin.site.register(AvisoIncumplimiento, AvisoIncumplimientoAdmin)
admin.site.register(Operacion, OperacionAdmin)
admin.site.register(EmpleadoDeInmobiliaria, EmpleadoDeInmobiliariaAdmin)
admin.site.register(Oficina, OficinaAdmin)
admin.site.register(VendedorFinaer, VendedorFinaerAdmin)