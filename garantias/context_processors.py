#coding=UTF-8
from website.models import *
from garantias.forms import ConsultaForm, BuscadorInmobiliariasForm


def acuerdos(request):
    acuerdos = Acuerdo.objects.all().order_by('fecha').reverse()
    if request.path != "/inmobiliarias/adheridas":
        acuerdos = acuerdos[:3]
    else:
        acuerdos = acuerdos[:6]
    return {'acuerdos': acuerdos}


def contacto(_):
    consultaForm = ConsultaForm()
    return {'consultaForm': consultaForm}


def buscadorInmobiliarias(_):
    buscadorInmobiliariasForm = BuscadorInmobiliariasForm()
    return {'buscadorInmobiliariasForm': buscadorInmobiliariasForm}