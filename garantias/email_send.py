# -*- coding: utf-8 -*-
from django.core import urlresolvers

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.forms.models import model_to_dict
from django.conf import settings
from website.models import DynamicContent


def send_consulta(consulta):
    subject = '[Finaer Web] Nueva consulta'
    data = model_to_dict(consulta)
    body_plain = render_to_string("email/consulta.txt", data)
    # body_html = render_to_string("email/consulta.html", data)
    from_email = settings.DEFAULT_FROM_EMAIL
    to_email = settings.NOTIFICATION_EMAILS
    reply_to = consulta.email
    email = EmailMultiAlternatives(subject, body_plain, from_email, to_email, headers={'Reply-To': reply_to})
    # email.attach_alternative(body_html, "text/html")
    return email.send()


#Begin email to oficina
def send_solicitud_garantia_oficina(solicitud_garantia, site, oficina):
    subject = '[Finaer Web] Nueva solicitud de garantia %s' % solicitud_garantia.nro  # Sin acentos por encoding
    solicitud_admin_url = site.domain + urlresolvers.reverse('admin:garantias_solicitudgarantia_change',
                                                             args=(solicitud_garantia.pk,))
    data = {'solicitud_admin_url': solicitud_admin_url}
    body_plain = render_to_string("email/solicitud_garantia_oficina.txt", data)
    body_html = render_to_string("email/solicitud_garantia_oficina.html", data)
    from_email = settings.DEFAULT_FROM_EMAIL
    to_email = oficina.email
    email = EmailMultiAlternatives(subject, body_plain, from_email, to_email)
    email.attach_alternative(body_html, "text/html")
    return email.send()


def send_modificacion_de_solicitud_garantia_oficina(solicitud_garantia, site, oficina):
    subject = '[Finaer Web] Nueva solicitud de garantia %s' % solicitud_garantia.nro  # Sin acentos por encoding
    solicitud_admin_url = site.domain + urlresolvers.reverse('admin:garantias_solicitudgarantia_change',
                                                             args=(solicitud_garantia.pk,))
    data = {'solicitud_admin_url': solicitud_admin_url}
    body_plain = render_to_string("email/modificacion_de_solicitud_garantia_oficina.txt", data)
    body_html = render_to_string("email/modificacion_de_solicitud_garantia_oficina.html", data)
    from_email = settings.DEFAULT_FROM_EMAIL
    to_email = (oficina.email, )
    email = EmailMultiAlternatives(subject, body_plain, from_email, to_email)
    email.attach_alternative(body_html, "text/html")
    return email.send()
#End email to oficina


#Begin email to casa_central
def send_inmobiliaria_casa_central(inmobiliaria, site):
    subject = '[Finaer Web] Nueva inmobiliaria %s' % inmobiliaria.pk  # Sin acentos por encoding
    inmobiliaria_admin_url = site.domain + urlresolvers.reverse('admin:garantias_inmobiliaria_change',
                                                             args=(inmobiliaria.pk,))
    data = {'inmobiliaria_admin_url': inmobiliaria_admin_url, 'inmobiliaria': inmobiliaria}
    body_plain = render_to_string("email/inmobiliaria_casa_central.txt", data)
    body_html = render_to_string("email/inmobiliaria_casa_central.html", data)
    from_email = settings.DEFAULT_FROM_EMAIL
    to_email = settings.NOTIFICATION_EMAILS
    email = EmailMultiAlternatives(subject, body_plain, from_email, to_email)
    email.attach_alternative(body_html, "text/html")
    return email.send()
#End email to casa_central


#Begin email to inquilino
def send_solicitud_garantia_inquilino(solicitud_garantia):
    subject = 'Finaer - Su solicitud de garantia fue recibida'  # Sin acentos por encoding
    working_hours = DynamicContent.objects.get(field='Horarios').value
    main_phone_number = DynamicContent.objects.get(field='Teléfono principal').value
    offices = DynamicContent.objects.get(field='Oficinas').value
    data = {'solicitud_nro': solicitud_garantia.nro,
            'nombre_inquilino': solicitud_garantia.inquilino.nombre_completo,
            'working_hours': working_hours,
            'contact_email': settings.CONTACT_EMAIL,
            'main_phone_number': main_phone_number,
            'offices': offices}
    body_plain = render_to_string("email/solicitud_garantia_inquilino.txt", data)
    body_html = render_to_string("email/solicitud_garantia_inquilino.html", data)
    from_email = settings.DEFAULT_FROM_EMAIL
    to_email = (solicitud_garantia.inquilino.email, )
    reply_to = settings.CONTACT_EMAIL
    email = EmailMultiAlternatives(subject, body_plain, from_email, to_email, headers={'Reply-To': reply_to})
    email.attach_alternative(body_html, "text/html")
    return email.send()


def send_modificacion_de_solicitud_garantia_inquilino(solicitud_garantia):
    subject = 'Finaer - Su solicitud de garantia fue recibida'  # Sin acentos por encoding
    working_hours = DynamicContent.objects.get(field='Horarios').value
    main_phone_number = DynamicContent.objects.get(field='Teléfono principal').value
    offices = DynamicContent.objects.get(field='Oficinas').value
    data = {'solicitud_nro': solicitud_garantia.nro,
            'nombre_inquilino': solicitud_garantia.inquilino.nombre_completo,
            'working_hours': working_hours,
            'contact_email': settings.CONTACT_EMAIL,
            'main_phone_number': main_phone_number,
            'offices': offices}
    body_plain = render_to_string("email/modificacion_de_solicitud_garantia_inquilino.txt", data)
    body_html = render_to_string("email/modificacion_de_solicitud_garantia_inquilino.html", data)
    from_email = settings.DEFAULT_FROM_EMAIL
    to_email = (solicitud_garantia.inquilino.email, )
    reply_to = settings.CONTACT_EMAIL
    email = EmailMultiAlternatives(subject, body_plain, from_email, to_email, headers={'Reply-To': reply_to})
    email.attach_alternative(body_html, "text/html")
    return email.send()
#End email to inquilino