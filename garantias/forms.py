# -*- coding: utf-8 -*-
import datetime

from django import forms
from django.forms.extras.widgets import SelectDateWidget
from django.contrib.localflavor.ar.forms import ARProvinceSelect
from garantias.models import *
from utils.widgets import SimpleRadioSelect
from utils.provincias_regiones import PROVINCIAS_REGIONES_CHOICES


years_range_birth = [y for y in range(1900,
                                      datetime.datetime.now().year)]
years_range_contrato = [y for y in range(datetime.datetime.now().year,
                                         datetime.datetime.now().year + 5)]
years_range_vencimiento = [y for y in range(2000,
                                            datetime.datetime.now().year)]

PROVINCIAS_REGIONES = [('ALL', u'Todas')] + list(PROVINCIAS_REGIONES_CHOICES)

def get_localidades():
    localidades = list(Localidad.objects.all().values_list('nombre', 'nombre'))
    localidades = [('', 'Seleccione...')] + localidades
    return localidades

class InmobiliariaField(forms.CharField):
    def to_python(self, value):
        if value:
            inmobiliarias_existentes = Inmobiliaria.objects.filter(nombre=value)
            if inmobiliarias_existentes:
                return inmobiliarias_existentes[0]
            else:
                Inmobiliaria.objects.create(nombre=value, origen='SUGERIDO_POR_INQUILINO')
        else:
            return None


class SolicitudGarantiaForm(forms.ModelForm):
    inmobiliaria = forms.CharField(required=False)

    def clean(self):
        return self.cleaned_data

    error_css_class = 'field-error'

    class Meta:
        model = SolicitudGarantia
        exclude = ('estado', 'inmobiliaria', 'oficina')


class ModificacionDeSolicitudGarantiaForm(forms.ModelForm):
    #forms.CharField(label=u'Nro. Solicitud', max_length=6)
    inmobiliaria = forms.CharField(required=False)

    def clean(self):
        return self.cleaned_data

    error_css_class = 'field-error'

    class Meta:
        model = SolicitudGarantia
        exclude = ('estado', 'inmobiliaria', 'oficina')


class ParticipanteSolicitudAdminForm(forms.ModelForm):
    class Meta:
        widgets = {
            'domicilio_provincia': ARProvinceSelect,
            'padres_domicilio_provincia': ARProvinceSelect,
        }


class InquilinoAdminForm(ParticipanteSolicitudAdminForm):
    class Meta(ParticipanteSolicitudAdminForm.Meta):
        model = ParticipanteSolicitud


class CosolicitanteAdminForm(ParticipanteSolicitudAdminForm):
    class Meta(ParticipanteSolicitudAdminForm.Meta):
        model = Cosolicitante


class SolicitudGarantiaInlineForm(forms.ModelForm):
    class Meta:
        model = SolicitudGarantia
        fields = []


class ParticipanteSolicitudForm(forms.ModelForm):
    SI_NO = (
        (True, 'Si'),
        (False, 'No'),
    )

    fecha_nacimiento = forms.DateField(widget=SelectDateWidget(attrs={'class': 'inline w70'},
                                                               years=years_range_birth), required=False)
    domicilio_provincia = forms.CharField(widget=ARProvinceSelect(), required=False)
    empleado_tipo = forms.ChoiceField(widget=SimpleRadioSelect,
                                      choices=ParticipanteSolicitud.EMPLEADO_TIPOS,
                                      required=False)

    error_css_class = 'field-error'

    class Meta:
        exclude = ('solicitud', )


class InquilinoForm(ParticipanteSolicitudForm):
    class Meta(ParticipanteSolicitudForm.Meta):
        model = Inquilino


class ModificacionDeInquilinoForm(ParticipanteSolicitudForm):
    class Meta(ParticipanteSolicitudForm.Meta):
        model = Inquilino
        exclude = ('tipo_doc', 'num_doc', 'solicitud', )


class InquilinoEncuestaForm(forms.ModelForm):
    class Meta:
        model = InquilinoEncuesta
        exclude = ('inquilino',)


class CosolicitanteForm(ParticipanteSolicitudForm):
    class Meta(ParticipanteSolicitudForm.Meta):
        model = Cosolicitante


class ComprobanteForm(forms.ModelForm):
    comprobante = forms.FileField()

    class Meta:
        model = Comprobante


class NroSolicitudForm(forms.Form):
    nro_solicitud = forms.CharField(label=u'Nro. Solicitud', max_length=6)

    def clean(self):
        nro_ingresado = self.cleaned_data.get('nro_solicitud')
        if nro_ingresado:
            if not SolicitudGarantia.objects.filter(nro=nro_ingresado).exists():
                raise forms.ValidationError(u"El número ingresado no corresponde "
                                            "a una solicitud existente.")
        return self.cleaned_data


class AvisoIncumplimientoAdminInlineForm(forms.ModelForm):
    class Meta:
        model = AvisoIncumplimiento
        exclude = ('nro_cert_garantia', )


class AvisoIncumplimientoForm(forms.ModelForm):
    fecha_vencido = forms.DateField(widget=SelectDateWidget(attrs={'class': 'inline w70'},
                                                            years=years_range_vencimiento))

    error_css_class = 'field-error'

    class Meta:
        model = AvisoIncumplimiento


class InmobiliariaAdhesionForm(forms.ModelForm):
    logo = forms.FileField(required=False)

    error_css_class = 'field-error'

    class Meta:
        model = Inmobiliaria
        exclude = ('localidades', 'aprobacion_estado', 'ubicacion')


class BuscadorInmobiliariasForm(forms.Form):
    provincia_region = forms.ChoiceField(choices=PROVINCIAS_REGIONES)
    localidad_barrio = forms.ChoiceField(required=False)
    nombre = forms.CharField(required=False, max_length=100)


class ConsultaForm(forms.ModelForm):
    class Meta:
        model = Consulta


class ComprobanteAdminInlineForm(forms.ModelForm):
    class Meta:
        model = Comprobante
        exclude = ('fecha', )


class AlquilerForm(forms.Form):
    provincia_region = forms.ChoiceField(choices=[('', u'Seleccione...')] + list(PROVINCIAS_REGIONES_CHOICES))
    localidad_barrio = forms.ChoiceField(choices=get_localidades())

    error_css_class = 'field-error'


class OficinaForm(forms.ModelForm):
    ubicacion = forms.CharField()

    error_css_class = 'field-error'

    class Meta:
        model = Oficina

