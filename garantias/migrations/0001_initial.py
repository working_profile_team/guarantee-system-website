# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'SolicitudGarantia'
        db.create_table('garantias_solicitudgarantia', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('ingresos_inquilino_monto', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('ingresos_inquilino_concepto', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('ingresos_conyugue_monto', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('ingresos_conyugue_concepto', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('alquiler_pactado', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('expensas_pactadas', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('vigencia_contrato_desde', self.gf('django.db.models.fields.DateField')(blank=True)),
            ('vigencia_contrato_hasta', self.gf('django.db.models.fields.DateField')(blank=True)),
            ('contrato_meses', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('garantias', ['SolicitudGarantia'])

        # Adding model 'Persona'
        db.create_table('garantias_persona', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre_completo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('tipo_doc', self.gf('django.db.models.fields.CharField')(default='DNI', max_length=20)),
            ('num_doc', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('cuit', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('fecha_nacimiento', self.gf('django.db.models.fields.DateField')()),
            ('nacionalidad', self.gf('django_countries.fields.CountryField')(max_length=2, null=True, blank=True)),
            ('estado_civil', self.gf('django.db.models.fields.IntegerField')(max_length=20, blank=True)),
            ('domicilio', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('domicilio_cp', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('domicilio_ciudad', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('domicilio_provincia', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('celular', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
        ))
        db.send_create_signal('garantias', ['Persona'])

        # Adding model 'Inquilino'
        db.create_table('garantias_inquilino', (
            ('persona_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['garantias.Persona'], unique=True, primary_key=True)),
            ('solicitud', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['garantias.SolicitudGarantia'])),
            ('vivienda_modo', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('vivienda_alq_actual', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('vivienda_antig_dom', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('hijos_tiene', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('hijos_cant', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('hijos_edades', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('hijos_conviven', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('hijos_cuota_alim', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('padres_nombres', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('padres_domicilio', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('padres_domicilio_cp', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('padres_domicilio_ciudad', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('padres_domicilio_provincia', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
        ))
        db.send_create_signal('garantias', ['Inquilino'])

        # Adding model 'Conyugue'
        db.create_table('garantias_conyugue', (
            ('persona_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['garantias.Persona'], unique=True, primary_key=True)),
            ('inquilino', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['garantias.Inquilino'], unique=True)),
        ))
        db.send_create_signal('garantias', ['Conyugue'])

        # Adding model 'InquilinoInfoLaboral'
        db.create_table('garantias_inquilinoinfolaboral', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('inquilino', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['garantias.Inquilino'], unique=True)),
            ('empleado_tipo', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('mat_profesional', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('empresa', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('actividad', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('domicilio', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('localidad', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('puesto', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('puesto_seccion', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('empresa_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('actividad_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('domicilio_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('localidad_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('telefono_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('condicion_iva', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('cuit', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
        ))
        db.send_create_signal('garantias', ['InquilinoInfoLaboral'])

        # Adding model 'Inmobiliaria'
        db.create_table('garantias_inmobiliaria', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('direccion', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('telefonos', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('sitio_web', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('responsable', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('garantias', ['Inmobiliaria'])

        # Adding model 'Consulta'
        db.create_table('garantias_consulta', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('nombre_completo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('tipo_alquiler', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('barrio_localidad', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('importe', self.gf('django.db.models.fields.IntegerField')()),
            ('consulta', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('garantias', ['Consulta'])


    def backwards(self, orm):
        
        # Deleting model 'SolicitudGarantia'
        db.delete_table('garantias_solicitudgarantia')

        # Deleting model 'Persona'
        db.delete_table('garantias_persona')

        # Deleting model 'Inquilino'
        db.delete_table('garantias_inquilino')

        # Deleting model 'Conyugue'
        db.delete_table('garantias_conyugue')

        # Deleting model 'InquilinoInfoLaboral'
        db.delete_table('garantias_inquilinoinfolaboral')

        # Deleting model 'Inmobiliaria'
        db.delete_table('garantias_inmobiliaria')

        # Deleting model 'Consulta'
        db.delete_table('garantias_consulta')


    models = {
        'garantias.consulta': {
            'Meta': {'object_name': 'Consulta'},
            'barrio_localidad': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'consulta': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importe': ('django.db.models.fields.IntegerField', [], {}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tipo_alquiler': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'garantias.conyugue': {
            'Meta': {'object_name': 'Conyugue', '_ormbases': ['garantias.Persona']},
            'inquilino': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['garantias.Inquilino']", 'unique': 'True'}),
            'persona_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['garantias.Persona']", 'unique': 'True', 'primary_key': 'True'})
        },
        'garantias.inmobiliaria': {
            'Meta': {'object_name': 'Inmobiliaria'},
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'responsable': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'sitio_web': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'telefonos': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'garantias.inquilino': {
            'Meta': {'object_name': 'Inquilino', '_ormbases': ['garantias.Persona']},
            'hijos_cant': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_conviven': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_cuota_alim': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_edades': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'hijos_tiene': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'padres_domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'padres_domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'padres_domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_nombres': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'persona_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['garantias.Persona']", 'unique': 'True', 'primary_key': 'True'}),
            'solicitud': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['garantias.SolicitudGarantia']"}),
            'vivienda_alq_actual': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vivienda_antig_dom': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'vivienda_modo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'garantias.inquilinoinfolaboral': {
            'Meta': {'object_name': 'InquilinoInfoLaboral'},
            'actividad': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'actividad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'condicion_iva': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'cuit': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'empleado_tipo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'empresa': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'empresa_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inquilino': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['garantias.Inquilino']", 'unique': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'localidad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'mat_profesional': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'puesto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_seccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        'garantias.persona': {
            'Meta': {'object_name': 'Persona'},
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'cuit': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'estado_civil': ('django.db.models.fields.IntegerField', [], {'max_length': '20', 'blank': 'True'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nacionalidad': ('django_countries.fields.CountryField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'num_doc': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tipo_doc': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20'})
        },
        'garantias.solicitudgarantia': {
            'Meta': {'object_name': 'SolicitudGarantia'},
            'alquiler_pactado': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'contrato_meses': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'expensas_pactadas': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingresos_conyugue_concepto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ingresos_conyugue_monto': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ingresos_inquilino_concepto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ingresos_inquilino_monto': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vigencia_contrato_desde': ('django.db.models.fields.DateField', [], {'blank': 'True'}),
            'vigencia_contrato_hasta': ('django.db.models.fields.DateField', [], {'blank': 'True'})
        }
    }

    complete_apps = ['garantias']
