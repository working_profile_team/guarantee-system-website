# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'LocalidadArgentina'
        db.create_table('garantias_localidadargentina', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('provincia_region', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('garantias', ['LocalidadArgentina'])

        # Deleting field 'Inmobiliaria.direccion'
        db.delete_column('garantias_inmobiliaria', 'direccion')

        # Adding field 'Inmobiliaria.logo'
        db.add_column('garantias_inmobiliaria', 'logo', self.gf('sorl.thumbnail.fields.ImageField')(default='hola', max_length=100), keep_default=False)

        # Adding field 'Inmobiliaria.domicilio'
        db.add_column('garantias_inmobiliaria', 'domicilio', self.gf('django.db.models.fields.CharField')(default='hola', max_length=100), keep_default=False)

        # Adding field 'Inmobiliaria.localidad'
        db.add_column('garantias_inmobiliaria', 'localidad', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['garantias.LocalidadArgentina']), keep_default=False)

        # Adding field 'Inmobiliaria.aprobacion_estado'
        db.add_column('garantias_inmobiliaria', 'aprobacion_estado', self.gf('django.db.models.fields.CharField')(default='NO_APROBADA', max_length=20), keep_default=False)


    def backwards(self, orm):
        
        # Deleting model 'LocalidadArgentina'
        db.delete_table('garantias_localidadargentina')

        # User chose to not deal with backwards NULL issues for 'Inmobiliaria.direccion'
        raise RuntimeError("Cannot reverse this migration. 'Inmobiliaria.direccion' and its values cannot be restored.")

        # Deleting field 'Inmobiliaria.logo'
        db.delete_column('garantias_inmobiliaria', 'logo')

        # Deleting field 'Inmobiliaria.domicilio'
        db.delete_column('garantias_inmobiliaria', 'domicilio')

        # Deleting field 'Inmobiliaria.localidad'
        db.delete_column('garantias_inmobiliaria', 'localidad_id')

        # Deleting field 'Inmobiliaria.aprobacion_estado'
        db.delete_column('garantias_inmobiliaria', 'aprobacion_estado')


    models = {
        'garantias.consulta': {
            'Meta': {'object_name': 'Consulta'},
            'barrio_localidad': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'consulta': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importe': ('django.db.models.fields.IntegerField', [], {}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tipo_alquiler': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'garantias.conyugue': {
            'Meta': {'object_name': 'Conyugue', '_ormbases': ['garantias.Persona']},
            'inquilino': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['garantias.Inquilino']", 'unique': 'True'}),
            'persona_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['garantias.Persona']", 'unique': 'True', 'primary_key': 'True'})
        },
        'garantias.inmobiliaria': {
            'Meta': {'object_name': 'Inmobiliaria'},
            'aprobacion_estado': ('django.db.models.fields.CharField', [], {'default': "'NO_APROBADA'", 'max_length': '20'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['garantias.LocalidadArgentina']"}),
            'logo': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'responsable': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'sitio_web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'telefonos': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'garantias.inquilino': {
            'Meta': {'object_name': 'Inquilino', '_ormbases': ['garantias.Persona']},
            'hijos_cant': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_conviven': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_cuota_alim': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_edades': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'hijos_tiene': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'padres_domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'padres_domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'padres_domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_nombres': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'persona_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['garantias.Persona']", 'unique': 'True', 'primary_key': 'True'}),
            'solicitud': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['garantias.SolicitudGarantia']"}),
            'vivienda_alq_actual': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vivienda_antig_dom': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'vivienda_modo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'garantias.inquilinoinfolaboral': {
            'Meta': {'object_name': 'InquilinoInfoLaboral'},
            'actividad': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'actividad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'condicion_iva': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'cuit': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'empleado_tipo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'empresa': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'empresa_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inquilino': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['garantias.Inquilino']", 'unique': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'localidad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'mat_profesional': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'puesto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_seccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        'garantias.localidadargentina': {
            'Meta': {'object_name': 'LocalidadArgentina'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'provincia_region': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'garantias.persona': {
            'Meta': {'object_name': 'Persona'},
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'cuit': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'estado_civil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nacionalidad': ('django_countries.fields.CountryField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'num_doc': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tipo_doc': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20'})
        },
        'garantias.solicitudgarantia': {
            'Meta': {'object_name': 'SolicitudGarantia'},
            'alquiler_pactado': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'contrato_meses': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'expensas_pactadas': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingresos_conyugue_concepto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ingresos_conyugue_monto': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ingresos_inquilino_concepto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ingresos_inquilino_monto': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vigencia_contrato_desde': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'vigencia_contrato_hasta': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['garantias']
