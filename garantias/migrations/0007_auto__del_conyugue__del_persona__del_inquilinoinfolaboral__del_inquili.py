# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting model 'Conyugue'
        db.delete_table('garantias_conyugue')

        # Deleting model 'Persona'
        db.delete_table('garantias_persona')

        # Deleting model 'InquilinoInfoLaboral'
        db.delete_table('garantias_inquilinoinfolaboral')

        # Deleting model 'Inquilino'
        db.delete_table('garantias_inquilino')

        # Adding model 'ParticipanteSolicitud'
        db.create_table('garantias_participantesolicitud', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('solicitud', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['garantias.SolicitudGarantia'])),
            ('nombre_completo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('tipo_doc', self.gf('django.db.models.fields.CharField')(default='DNI', max_length=20)),
            ('num_doc', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('fecha_nacimiento', self.gf('django.db.models.fields.DateField')()),
            ('nacionalidad', self.gf('django_countries.fields.CountryField')(max_length=2, null=True, blank=True)),
            ('estado_civil', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('domicilio_cp', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('domicilio_ciudad', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('domicilio_provincia', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('celular', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('vivienda_modo', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('vivienda_alq_actual', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('vivienda_antig_dom', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('hijos_tiene', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('hijos_cant', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('hijos_edades', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('hijos_conviven', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('hijos_cuota_alim', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('padres_nombres', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('padres_domicilio', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('padres_domicilio_cp', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('padres_domicilio_ciudad', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('padres_domicilio_provincia', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('empleado_tipo', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('mat_profesional', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('empresa', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('actividad', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('domicilio', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('localidad', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('puesto', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('puesto_seccion', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('empresa_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('actividad_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('domicilio_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('localidad_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('telefono_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('condicion_iva', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('cuit', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('ingresos_monto', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('ingresos_concepto', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal('garantias', ['ParticipanteSolicitud'])

        # Deleting field 'SolicitudGarantia.ingresos_conyugue_monto'
        db.delete_column('garantias_solicitudgarantia', 'ingresos_conyugue_monto')

        # Deleting field 'SolicitudGarantia.ingresos_inquilino_concepto'
        db.delete_column('garantias_solicitudgarantia', 'ingresos_inquilino_concepto')

        # Deleting field 'SolicitudGarantia.ingresos_inquilino_monto'
        db.delete_column('garantias_solicitudgarantia', 'ingresos_inquilino_monto')

        # Deleting field 'SolicitudGarantia.ingresos_conyugue_concepto'
        db.delete_column('garantias_solicitudgarantia', 'ingresos_conyugue_concepto')


    def backwards(self, orm):
        
        # Adding model 'Conyugue'
        db.create_table('garantias_conyugue', (
            ('inquilino', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['garantias.Inquilino'], unique=True)),
            ('persona_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['garantias.Persona'], unique=True, primary_key=True)),
        ))
        db.send_create_signal('garantias', ['Conyugue'])

        # Adding model 'Persona'
        db.create_table('garantias_persona', (
            ('fecha_nacimiento', self.gf('django.db.models.fields.DateField')()),
            ('nacionalidad', self.gf('django_countries.fields.CountryField')(max_length=2, null=True, blank=True)),
            ('cuit', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('domicilio_ciudad', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('tipo_doc', self.gf('django.db.models.fields.CharField')(default='DNI', max_length=20)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre_completo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('domicilio_provincia', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('celular', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('num_doc', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('estado_civil', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('domicilio_cp', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('domicilio', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal('garantias', ['Persona'])

        # Adding model 'InquilinoInfoLaboral'
        db.create_table('garantias_inquilinoinfolaboral', (
            ('empresa', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('condicion_iva', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('puesto_seccion', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('cuit', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('actividad', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('inquilino', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['garantias.Inquilino'], unique=True)),
            ('empresa_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('localidad_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('puesto', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('localidad', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('mat_profesional', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('domicilio_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('telefono_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('actividad_ocu_anterior', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('domicilio', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('empleado_tipo', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
        ))
        db.send_create_signal('garantias', ['InquilinoInfoLaboral'])

        # Adding model 'Inquilino'
        db.create_table('garantias_inquilino', (
            ('padres_nombres', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('padres_domicilio_ciudad', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('hijos_cuota_alim', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('vivienda_modo', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('hijos_edades', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('hijos_conviven', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('vivienda_antig_dom', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('hijos_cant', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('persona_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['garantias.Persona'], unique=True, primary_key=True)),
            ('padres_domicilio_provincia', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('solicitud', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['garantias.SolicitudGarantia'])),
            ('hijos_tiene', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('padres_domicilio_cp', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('vivienda_alq_actual', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('padres_domicilio', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
        ))
        db.send_create_signal('garantias', ['Inquilino'])

        # Deleting model 'ParticipanteSolicitud'
        db.delete_table('garantias_participantesolicitud')

        # Adding field 'SolicitudGarantia.ingresos_conyugue_monto'
        db.add_column('garantias_solicitudgarantia', 'ingresos_conyugue_monto', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True), keep_default=False)

        # Adding field 'SolicitudGarantia.ingresos_inquilino_concepto'
        db.add_column('garantias_solicitudgarantia', 'ingresos_inquilino_concepto', self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True), keep_default=False)

        # Adding field 'SolicitudGarantia.ingresos_inquilino_monto'
        db.add_column('garantias_solicitudgarantia', 'ingresos_inquilino_monto', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True), keep_default=False)

        # Adding field 'SolicitudGarantia.ingresos_conyugue_concepto'
        db.add_column('garantias_solicitudgarantia', 'ingresos_conyugue_concepto', self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True), keep_default=False)


    models = {
        'garantias.consulta': {
            'Meta': {'object_name': 'Consulta'},
            'barrio_localidad': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'consulta': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importe': ('django.db.models.fields.IntegerField', [], {}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tipo_alquiler': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'garantias.inmobiliaria': {
            'Meta': {'object_name': 'Inmobiliaria'},
            'aprobacion_estado': ('django.db.models.fields.CharField', [], {'default': "'PENDIENTE'", 'max_length': '20'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'fecha_alta': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['garantias.LocalidadArgentina']", 'null': 'True'}),
            'logo': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'responsable': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'sitio_web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'telefonos': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'garantias.localidadargentina': {
            'Meta': {'object_name': 'LocalidadArgentina'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'provincia_region': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'garantias.participantesolicitud': {
            'Meta': {'object_name': 'ParticipanteSolicitud'},
            'actividad': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'actividad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'condicion_iva': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'cuit': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'domicilio_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'empleado_tipo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'empresa': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'empresa_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'estado_civil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {}),
            'hijos_cant': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_conviven': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_cuota_alim': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_edades': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'hijos_tiene': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingresos_concepto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ingresos_monto': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'localidad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'mat_profesional': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'nacionalidad': ('django_countries.fields.CountryField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'num_doc': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'padres_domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'padres_domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'padres_domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_nombres': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_seccion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'solicitud': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['garantias.SolicitudGarantia']"}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'tipo_doc': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20'}),
            'vivienda_alq_actual': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vivienda_antig_dom': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'vivienda_modo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'garantias.solicitudgarantia': {
            'Meta': {'object_name': 'SolicitudGarantia'},
            'alquiler_pactado': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'contrato_meses': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'expensas_pactadas': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vigencia_contrato_desde': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'vigencia_contrato_hasta': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['garantias']
