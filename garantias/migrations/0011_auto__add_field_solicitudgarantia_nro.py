# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'SolicitudGarantia.nro'
        db.add_column('garantias_solicitudgarantia', 'nro', self.gf('django.db.models.fields.CharField')(max_length=6, unique=True, null=True, blank=True), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'SolicitudGarantia.nro'
        db.delete_column('garantias_solicitudgarantia', 'nro')


    models = {
        'garantias.consulta': {
            'Meta': {'object_name': 'Consulta'},
            'barrio_localidad': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'consulta': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importe': ('django.db.models.fields.IntegerField', [], {}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'tipo_alquiler': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'garantias.cosolicitante': {
            'Meta': {'object_name': 'Cosolicitante'},
            'actividad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'actividad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'condicion_iva': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'cuit': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'domicilio_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'empleado_tipo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'empresa_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'empresa_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'estado_civil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {}),
            'hijos_cant': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_conviven': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_cuota_alim': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_edades': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'hijos_tiene': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingresos_concepto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ingresos_monto': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'localidad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'localidad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'mat_profesional': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'nacionalidad': ('django_countries.fields.CountryField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'num_doc': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'padres_domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'padres_domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'padres_domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_nombres': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_seccion_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'solicitud': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cosolicitantes'", 'to': "orm['garantias.SolicitudGarantia']"}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'tipo_doc': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20'}),
            'vivienda_alq_actual': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vivienda_antig_dom': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'vivienda_modo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'garantias.inmobiliaria': {
            'Meta': {'object_name': 'Inmobiliaria'},
            'aprobacion_estado': ('django.db.models.fields.CharField', [], {'default': "'PENDIENTE'", 'max_length': '20'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'fecha_alta': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['garantias.LocalidadArgentina']", 'null': 'True'}),
            'logo': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'responsable': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'sitio_web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'telefonos': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'garantias.inquilino': {
            'Meta': {'object_name': 'Inquilino'},
            'actividad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'actividad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'condicion_iva': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'cuit': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'domicilio_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'empleado_tipo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'empresa_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'empresa_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'estado_civil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {}),
            'hijos_cant': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_conviven': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_cuota_alim': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_edades': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'hijos_tiene': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingresos_concepto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ingresos_monto': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'localidad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'localidad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'mat_profesional': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'nacionalidad': ('django_countries.fields.CountryField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'num_doc': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'padres_domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'padres_domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'padres_domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_nombres': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_seccion_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'solicitud': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'inquilino'", 'unique': 'True', 'to': "orm['garantias.SolicitudGarantia']"}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'tipo_doc': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20'}),
            'vivienda_alq_actual': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vivienda_antig_dom': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'vivienda_modo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'garantias.localidadargentina': {
            'Meta': {'object_name': 'LocalidadArgentina'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'provincia_region': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'garantias.solicitudgarantia': {
            'Meta': {'object_name': 'SolicitudGarantia'},
            'alquiler_pactado': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'contrato_meses': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'expensas_pactadas': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nro': ('django.db.models.fields.CharField', [], {'max_length': '6', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'vigencia_contrato_desde': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'vigencia_contrato_hasta': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['garantias']
