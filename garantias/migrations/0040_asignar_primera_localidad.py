# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        #Add first localidad to ubicacion
        for obj in orm.Inmobiliaria.objects.all():
            if (not obj.ubicacion) and (obj.localidades.count() > 0):
                obj.ubicacion = obj.localidades.all()[0]
                obj.save()

    def backwards(self, orm):
        raise RuntimeError("Cannot reverse this migration.")

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'garantias.avisoincumplimiento': {
            'Meta': {'object_name': 'AvisoIncumplimiento'},
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fecha_vencido': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'locador': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'locatario': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'monto': ('django.db.models.fields.IntegerField', [], {}),
            'nro_cert_garantia': ('django.db.models.fields.CharField', [], {'max_length': '6', 'blank': 'True'}),
            'solicitud': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['garantias.SolicitudGarantia']", 'null': 'True', 'blank': 'True'})
        },
        'garantias.comprobante': {
            'Meta': {'object_name': 'Comprobante'},
            'comprobante': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'solicitud': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['garantias.SolicitudGarantia']", 'null': 'True', 'blank': 'True'})
        },
        'garantias.consulta': {
            'Meta': {'object_name': 'Consulta'},
            'consulta': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'garantias.cosolicitante': {
            'Meta': {'object_name': 'Cosolicitante'},
            'actividad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'actividad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'antiguedad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'condicion_iva': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'cuit': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'domicilio_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'empleado_tipo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'empresa_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'empresa_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'estado_civil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_cant': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_conviven': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_cuota_alim': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_edades': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'hijos_tiene': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingresos_concepto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ingresos_monto': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'localidad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'localidad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'mat_profesional': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'nacionalidad': ('django_countries.fields.CountryField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nombre_completo_conyuge': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'num_doc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'num_doc_conyuge': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'padres_domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'padres_domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_nombres': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_seccion_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'solicitud': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cosolicitantes'", 'null': 'True', 'to': "orm['garantias.SolicitudGarantia']"}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'tipo_doc': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20', 'blank': 'True'}),
            'tipo_doc_conyuge': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20', 'blank': 'True'}),
            'vivienda_alq_actual': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vivienda_antig_dom': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'vivienda_modo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'garantias.inmobiliaria': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Inmobiliaria'},
            'aprobacion_estado': ('django.db.models.fields.CharField', [], {'default': "'PENDIENTE'", 'max_length': '20'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'fecha_alta': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'generado_por': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidades': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'inmobiliarias'", 'null': 'True', 'to': "orm['garantias.Localidad']"}),
            'logo': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'origen': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'responsable': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'sitio_web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'telefonos': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ubicacion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['garantias.Localidad']", 'null': 'True'})
        },
        'garantias.inquilino': {
            'Meta': {'object_name': 'Inquilino'},
            'actividad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'actividad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'antiguedad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'condicion_iva': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'cuit': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'domicilio_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'empleado_tipo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'empresa_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'empresa_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'estado_civil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_cant': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_conviven': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_cuota_alim': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'hijos_edades': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'hijos_tiene': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingresos_concepto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ingresos_monto': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'localidad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'localidad_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'mat_profesional': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'nacionalidad': ('django_countries.fields.CountryField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nombre_completo_conyuge': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'num_doc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'num_doc_conyuge': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'padres_domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_domicilio_cp': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'padres_domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'padres_nombres': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_seccion_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'solicitud': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'inquilino'", 'unique': 'True', 'null': 'True', 'to': "orm['garantias.SolicitudGarantia']"}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_anterior': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'tipo_doc': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20', 'blank': 'True'}),
            'tipo_doc_conyuge': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20', 'blank': 'True'}),
            'vivienda_alq_actual': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'vivienda_antig_dom': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'vivienda_modo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        'garantias.inquilinoencuesta': {
            'Meta': {'object_name': 'InquilinoEncuesta'},
            'como_nos_conocio': ('django.db.models.fields.CharField', [], {'default': "'INMOBILIARIA'", 'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inquilino': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'encuesta_inquilino'", 'unique': 'True', 'to': "orm['garantias.Inquilino']"})
        },
        'garantias.localidad': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Localidad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'provincia_region': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'garantias.operacion': {
            'Meta': {'object_name': 'Operacion'},
            'anticipo': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cantidad_cuotas': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'codigo': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importe_cuota': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'solicitud': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'operacion'", 'unique': 'True', 'null': 'True', 'to': "orm['garantias.SolicitudGarantia']"})
        },
        'garantias.regionderepresentantes': {
            'Meta': {'object_name': 'RegionDeRepresentantes'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'garantias.representante': {
            'Meta': {'ordering': "['region']", 'object_name': 'Representante'},
            'codigo_postal': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'representante'", 'null': 'True', 'to': "orm['garantias.Localidad']"}),
            'logo': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['garantias.RegionDeRepresentantes']"}),
            'sitio_web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'telefonos': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'garantias.solicitudgarantia': {
            'Meta': {'object_name': 'SolicitudGarantia'},
            'alquiler_pactado': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'alquiler_pactado_segundo_anio': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'alquiler_pactado_tercer_anio': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'contrato_meses': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'estado': ('django.db.models.fields.CharField', [], {'default': "'ACTIVA'", 'max_length': '20'}),
            'expensas_pactadas': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inmobiliaria': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'solicitudes'", 'null': 'True', 'to': "orm['garantias.Inmobiliaria']"}),
            'nro': ('django.db.models.fields.CharField', [], {'max_length': '6', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'tipo_solicitud': ('django.db.models.fields.CharField', [], {'default': "'PARTICULAR'", 'max_length': '20'}),
            'vendedor': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'solicitudes'", 'null': 'True', 'to': "orm['auth.User']"}),
            'vigencia_contrato_desde': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'vigencia_contrato_hasta': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['garantias']
    symmetrical = True
