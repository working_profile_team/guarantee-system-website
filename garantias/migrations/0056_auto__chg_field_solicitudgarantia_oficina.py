# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'SolicitudGarantia.oficina'
        db.alter_column(u'garantias_solicitudgarantia', 'oficina_id', self.gf('django.db.models.fields.related.ForeignKey')(default=-1, on_delete=models.PROTECT, to=orm['garantias.Oficina']))

    def backwards(self, orm):

        # Changing field 'SolicitudGarantia.oficina'
        db.alter_column(u'garantias_solicitudgarantia', 'oficina_id', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['garantias.Oficina'], on_delete=models.PROTECT))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'garantias.avisoincumplimiento': {
            'Meta': {'object_name': 'AvisoIncumplimiento'},
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fecha_vencido': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'locador': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'locatario': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'monto': ('django.db.models.fields.IntegerField', [], {}),
            'nro_cert_garantia': ('django.db.models.fields.CharField', [], {'max_length': '6', 'blank': 'True'}),
            'solicitud': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['garantias.SolicitudGarantia']", 'null': 'True', 'blank': 'True'})
        },
        u'garantias.comprobante': {
            'Meta': {'object_name': 'Comprobante'},
            'comprobante': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'solicitud': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['garantias.SolicitudGarantia']", 'null': 'True', 'blank': 'True'})
        },
        u'garantias.consulta': {
            'Meta': {'object_name': 'Consulta'},
            'consulta': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'garantias.cosolicitante': {
            'Meta': {'object_name': 'Cosolicitante'},
            'actividad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'antiguedad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'cuit': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'empleado_tipo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'empresa_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'estado_civil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingresos_monto': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'localidad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'nacionalidad': ('django_countries.fields.CountryField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nombre_completo_conyuge': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'num_doc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'num_doc_conyuge': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'puesto_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_seccion_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'solicitud': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cosolicitantes'", 'null': 'True', 'to': u"orm['garantias.SolicitudGarantia']"}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'tipo_doc': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20', 'blank': 'True'}),
            'tipo_doc_conyuge': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20', 'blank': 'True'})
        },
        u'garantias.empleadodeinmobiliaria': {
            'Meta': {'object_name': 'EmpleadoDeInmobiliaria'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inmobiliaria': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'empleados'", 'to': u"orm['garantias.Inmobiliaria']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nro_interno': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'})
        },
        u'garantias.inmobiliaria': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Inmobiliaria'},
            'aprobacion_estado': ('django.db.models.fields.CharField', [], {'default': "'PENDIENTE'", 'max_length': '20'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'fecha_alta': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'generado_por': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'on_delete': 'models.PROTECT', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidades': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'inmobiliarias'", 'null': 'True', 'to': u"orm['garantias.Localidad']"}),
            'logo': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'origen': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'responsable': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'sitio_web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'telefonos': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ubicacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['garantias.Localidad']", 'null': 'True', 'on_delete': 'models.PROTECT'})
        },
        u'garantias.inquilino': {
            'Meta': {'object_name': 'Inquilino'},
            'actividad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'antiguedad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'cuit': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_ciudad': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'domicilio_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio_provincia': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'empleado_tipo': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'empresa_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'estado_civil': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'fecha_nacimiento': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingresos_monto': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'localidad_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'nacionalidad': ('django_countries.fields.CountryField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            'nombre_completo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nombre_completo_conyuge': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'num_doc': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'num_doc_conyuge': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'puesto_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'puesto_seccion_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'solicitud': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'inquilino'", 'unique': 'True', 'null': 'True', 'to': u"orm['garantias.SolicitudGarantia']"}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'telefono_ocu_actual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'tipo_doc': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20', 'blank': 'True'}),
            'tipo_doc_conyuge': ('django.db.models.fields.CharField', [], {'default': "'DNI'", 'max_length': '20', 'blank': 'True'})
        },
        u'garantias.inquilinoencuesta': {
            'Meta': {'object_name': 'InquilinoEncuesta'},
            'como_nos_conocio': ('django.db.models.fields.CharField', [], {'default': "'INMOBILIARIA'", 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inquilino': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'encuesta_inquilino'", 'unique': 'True', 'to': u"orm['garantias.Inquilino']"})
        },
        u'garantias.localidad': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Localidad'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'provincia_region': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'garantias.oficina': {
            'Meta': {'ordering': "['nombre']", 'object_name': 'Oficina'},
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidades': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'oficinas'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['garantias.Localidad']"}),
            'logo': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'publicada': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'telefonos': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ubicacion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['garantias.Localidad']", 'null': 'True', 'on_delete': 'models.PROTECT'})
        },
        u'garantias.operacion': {
            'Meta': {'object_name': 'Operacion'},
            'anticipo': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cantidad_cuotas': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'codigo': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'fecha_firmada': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'firmada': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importe_cuota': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'solicitud': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'operacion'", 'unique': 'True', 'null': 'True', 'to': u"orm['garantias.SolicitudGarantia']"})
        },
        u'garantias.solicitudgarantia': {
            'Meta': {'object_name': 'SolicitudGarantia'},
            'alquiler_pactado': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'alquiler_pactado_segundo_anio': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'alquiler_pactado_tercer_anio': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'contrato_meses': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'estado': ('django.db.models.fields.CharField', [], {'default': "'ACTIVA'", 'max_length': '20'}),
            'expensas_pactadas': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inmobiliaria': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'solicitudes'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': u"orm['garantias.Inmobiliaria']"}),
            'nro': ('django.db.models.fields.CharField', [], {'max_length': '6', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'oficina': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'solicitudes'", 'on_delete': 'models.PROTECT', 'to': u"orm['garantias.Oficina']"}),
            'ultima_modificacion': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'vendedor': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'solicitudes'", 'null': 'True', 'on_delete': 'models.PROTECT', 'to': u"orm['auth.User']"})
        },
        u'garantias.vendedorfinaer': {
            'Meta': {'object_name': 'VendedorFinaer'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'oficina': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'vendedores'", 'to': u"orm['garantias.Oficina']"}),
            'usuario': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'vendedor'", 'unique': 'True', 'to': u"orm['auth.User']"})
        }
    }

    complete_apps = ['garantias']