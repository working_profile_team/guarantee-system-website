#coding=UTF-8
import datetime
from django.core.exceptions import ObjectDoesNotExist

from django.db import models
from django_countries import CountryField
from django.contrib.auth.models import User

from sorl.thumbnail import ImageField

from utils import friendly_id
from utils.provincias_regiones import PROVINCIAS_REGIONES_CHOICES
from django.utils import formats as django_formats


# Override del nombre a mostrar del user
def user_unicode(user):
    if user.first_name or user.last_name:
        return u'%s %s (%s)' % (user.first_name, user.last_name, user.username)
    else:
        return u'%s' % user.username


User.__unicode__ = user_unicode


PERMISSION_GROUPS = dict({
    'VENDEDOR_DE_SOLICITUDES': u'Vendedor de Solicitudes',
    'APRUEBA_SOLICITUD_DE_GARANTIAS': u'Aprueba Solicitud de Garantia',
    'ADMINISTRADOR_DE_SOLICITUDES': u'Administrador de Solicitudes',
    'ADMINISTRADOR_DE_OPERACIONES': u'Administrador de Operaciones',
    'ADMINISTRADOR_DE_INMOBILIARIAS': u'Administrador de Inmobiliarias',
    'APRUEBA_INMOBILIARIAS': u'Aprueba Inmobiliarias',
    'PROMOTOR_DE_INMOBILIARIAS': u'Promotor de Inmobiliarias',
    'ADMINISTRADOR_DE_OFICINAS': u'Administrador de Oficina',
})


class GarantiasUser(User):
    @classmethod
    def create_from(cls, user):
        return GarantiasUser.objects.get(pk=user.pk)

    def es_administrador(self):
        return self.is_superuser

    def es_vendedor_de_solicitudes(self):
        return self.groups.filter(name=PERMISSION_GROUPS['VENDEDOR_DE_SOLICITUDES']).exists() and hasattr(self, 'vendedor')

    def es_el_vendedor(self, un_vendedor):
        if un_vendedor is None or not self.es_vendedor_de_solicitudes():
            return False
        else:
            return self.pk == un_vendedor.pk

    def es_vendedor_de_solicitudes_en_oficina(self, una_oficina):
        return self.es_vendedor_de_solicitudes() and una_oficina.vendedores.filter(usuario__pk=self.pk).exists()

    def es_aprueba_solicitud_de_garantia(self):
        return (self.groups.filter(name=PERMISSION_GROUPS['APRUEBA_SOLICITUD_DE_GARANTIAS']).exists() and
                self.groups.filter(name=PERMISSION_GROUPS['ADMINISTRADOR_DE_SOLICITUDES']).exists())

    def es_administrador_de_solicitudes(self):
        return self.groups.filter(name=PERMISSION_GROUPS['ADMINISTRADOR_DE_SOLICITUDES']).exists()

    def es_administrador_de_operaciones(self):
        return self.groups.filter(name=PERMISSION_GROUPS['ADMINISTRADOR_DE_OPERACIONES']).exists()

    def es_administrador_de_inmobiliarias(self):
        return self.groups.filter(name=PERMISSION_GROUPS['ADMINISTRADOR_DE_INMOBILIARIAS']).exists()

    def es_el_promotor(self, un_promotor):
        return self.pk == un_promotor.pk

    def es_aprueba_inmobiliarias(self):
        return (self.groups.filter(name=PERMISSION_GROUPS['APRUEBA_INMOBILIARIAS']).exists() and
                self.groups.filter(name=PERMISSION_GROUPS['ADMINISTRADOR_DE_INMOBILIARIAS']).exists())

    def es_promotor_de_inmobiliarias(self):
        return self.groups.filter(name=PERMISSION_GROUPS['PROMOTOR_DE_INMOBILIARIAS']).exists()

    def es_administrador_de_oficinas(self):
        return self.groups.filter(name=PERMISSION_GROUPS['ADMINISTRADOR_DE_OFICINAS']).exists()

    class Meta:
        proxy = True


class SolicitudGarantia(models.Model):
    ESTADOS_SOLICITUD = (
        ('ACTIVA', u'Activa'),              #Solicitud en poder de la oficina
        ('INCOMPLETA', u'Incompleta'),      #Solicitud en poder de la oficina
        ('ENPROCESO', u'En proceso'),       #Solicitud en poder de la oficina
        ('ARESOLUCION', u'A Resolución'),   #Solicitud en poder de casa central
        ('PREAPROBADA', u'Pre aprobada'),   #Solicitud en poder de casa central
        ('APROBADA', u'Aprobada'),          #Solicitud en poder de casa central
        ('RECHAZADA', u'Rechazada'),        #Solicitud en poder de casa central
    )

    fecha = models.DateTimeField(u'fecha', auto_now_add=True)

    nro = models.CharField(u'nro', max_length=6, null=True, blank=True,
                           unique=True, editable=False)

    alquiler_pactado = models.IntegerField(u'alquiler pactado (primer año)', blank=True, null=True)
    alquiler_pactado_segundo_anio = models.IntegerField(u'alquiler pactado (segundo año)', blank=True, null=True)
    alquiler_pactado_tercer_anio = models.IntegerField(u'alquiler pactado (tercer año)', blank=True, null=True)
    expensas_pactadas = models.IntegerField(u'expensas pactadas', blank=True,
                                            null=True)
    contrato_meses = models.IntegerField(u'total meses de contrato', blank=True,
                                         null=True)
    vendedor = models.ForeignKey(User, blank=True, null=True,
                                 verbose_name=u'vendedor de Finaer',
                                 related_name='solicitudes',
                                 on_delete=models.PROTECT)
    inmobiliaria = models.ForeignKey('Inmobiliaria', blank=True, null=True,
                                     verbose_name=u'inmobiliaria',
                                     related_name='solicitudes',
                                     on_delete=models.SET_NULL)
    estado = models.CharField(u'estado', max_length=20,
                              choices=ESTADOS_SOLICITUD, default='ACTIVA')
    observaciones = models.TextField(u'observaciones', blank=True, null=True)
    oficina = models.ForeignKey('Oficina', related_name='solicitudes', verbose_name=u'oficina',
                                on_delete=models.PROTECT)
    ultima_modificacion = models.DateTimeField(u'última modificación', auto_now=True)

    def save(self, *args, **kwargs):
        super(SolicitudGarantia, self).save(*args, **kwargs)
        if not self.nro:
            self.nro = friendly_id.encode(self.id)
            super(SolicitudGarantia, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u'solicitud de garantía'
        verbose_name_plural = u'solicitudes de garantía'

    def __unicode__(self):
        return u'Solicitud Nro. %s' % self.nro

    def get_fecha_de_alta(self):
        return self.fecha.strftime('%d/%m/%Y')

    def get_inquilino_nombre_completo(self):
        try:
            inquilino = self.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if inquilino.nombre_completo:
            return u'%s' % inquilino.nombre_completo
        else:
            return u'Sin nombre'

    def get_inquilino_num_doc(self):
        try:
            inquilino = self.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if inquilino.num_doc:
            return u'%s' % inquilino.num_doc
        else:
            return u'Sin documento'

    def get_inquilino_telefono(self):
        try:
            inquilino = self.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if inquilino.telefono:
            return u'%s' % inquilino.telefono
        else:
            return u'Sin teléfono'

    def get_inquilino_celular(self):
        try:
            inquilino = self.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if inquilino.celular:
            return u'%s' % inquilino.celular
        else:
            return u'Sin Celular'

    def get_inquilino_telefono_y_celular(self):
        return self.get_inquilino_telefono()+' / '+self.get_inquilino_celular()

    def get_inmobiliaria(self):
        try:
            inmobiliaria = self.inmobiliaria
        except Inmobiliaria.DoesNotExist:
            return u'Sin inmobiliaria'
        if inmobiliaria:
            return u'%s' % inmobiliaria
        else:
            return u'Ninguna'

    def get_domicilio(self):
        try:
            inquilino = self.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if not inquilino.domicilio and not inquilino.domicilio_ciudad:
            return 'No asignado'
        return '%s (%s)' % (inquilino.domicilio, inquilino.domicilio_ciudad)

    def get_domicilio_provincia(self):
        try:
            inquilino = self.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if not inquilino.domicilio_provincia:
            return u'N/D'
        return u'%s' % inquilino.domicilio_provincia

    def get_domicilio_ciudad(self):
        try:
            inquilino = self.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if not inquilino.domicilio_ciudad:
            return u'N/D'
        return u'%s' % inquilino.domicilio_ciudad

    def get_ultima_modificacion(self):
        return self.ultima_modificacion.strftime('%d/%m/%Y %H:%M hs')

    def get_vendedor(self):
        try:
            vendedor = self.vendedor
        except ObjectDoesNotExist:
            return u'Sin Vendedor'
        if vendedor:
            return u'%s' % vendedor
        else:
            return u'No asignado'

    def get_oficina(self):
        oficina = self.oficina
        if not oficina:
            return u'No asignada'
        return u'%s' % oficina.nombre

    def clean(self):
        if self.vendedor and self.oficina:
            usuario_vendedor = self.vendedor
            if usuario_vendedor.vendedor.oficina != self.oficina:
                from django.core.exceptions import ValidationError
                raise ValidationError(u"El vendedor de Finaer seleccionado debe trabajar en la oficina asignada.")


class ParticipanteSolicitud(models.Model):
    TIPOS_DOC = (
        ('DNI', u'DNI'),
        ('LC', u'L.C.'),
    )
    ESTADOS_CIVILES = (
        ('CASADO', u'Casado'),
        ('SOLTERO', u'Soltero'),
        ('DIVORCIADO', u'Divorciado'),
        ('VIUDO', u'Viudo'),
    )
    VIVIENDA_MODOS = (
        ('PROPIA', u'Propia'),
        ('ALQUILADA', u'Alquilada'),
    )
    EMPLEADO_TIPOS = (
        ('EMPLEADO', u'Empleado'),
        ('COMERCIANTE', u'Comerciante'),
        ('INDEPENDIENTE', u'Profesional Independiente'),
    )
    CONDICIONES_IVA = (
        ('RESPINSC', u'Responsable Inscripto'),
        ('RESPNOINSC', u'Responsable No Inscripto'),
        ('EXENTO', u'Exento'),
        ('CONSFINAL', u'Consumidor Final'),
        ('MONOTRIBUTO', u'Monotributo'),
    )

    nombre_completo = models.CharField(u'apellido y nombre', max_length=100)
    tipo_doc = models.CharField(u'tipo de doc.', max_length=20,
                                choices=TIPOS_DOC, default='DNI', blank=True)
    num_doc = models.CharField(u'nro. de documento', max_length=50, blank=True)
    cuit = models.CharField(u'CUIT', max_length=50, blank=True)
    fecha_nacimiento = models.DateField(u'fecha de nacimiento', blank=True, null=True)
    nacionalidad = CountryField(u'nacionalidad', blank=True, null=True)
    estado_civil = models.CharField(u'estado civil', max_length=20,
                                    choices=ESTADOS_CIVILES, blank=True)
    domicilio = models.CharField(u'domicilio actual', max_length=100, blank=True)
    domicilio_ciudad = models.CharField(u'ciudad', max_length=50, blank=True)
    domicilio_provincia = models.CharField(u'provincia', max_length=50, blank=True)
    telefono = models.CharField(u'telefono', max_length=50, blank=True)
    celular = models.CharField(u'celular', max_length=50, blank=True)
    email = models.EmailField(u'email')
    nombre_completo_conyuge = models.CharField(u'apellido y nombre cónyuge', max_length=100, blank=True)
    tipo_doc_conyuge = models.CharField(u'tipo de doc. cónyuge', max_length=20,
                                        choices=TIPOS_DOC, default='DNI', blank=True)
    num_doc_conyuge = models.CharField(u'nro. de documento cónyuge', max_length=50, blank=True)
    empleado_tipo = models.CharField(u'tipo de empleado', max_length=20,
                                     choices=EMPLEADO_TIPOS, blank=True)
    empresa_ocu_actual = models.CharField(u'empresa', max_length=100,
                                          blank=True)
    actividad_ocu_actual = models.CharField(u'actividad', max_length=100,
                                            blank=True)
    domicilio_ocu_actual = models.CharField(u'domicilio', max_length=100,
                                            blank=True)
    localidad_ocu_actual = models.CharField(u'localidad', max_length=100,
                                            blank=True)
    telefono_ocu_actual = models.CharField(u'teléfono', max_length=50,
                                           blank=True)
    puesto_ocu_actual = models.CharField(u'puesto / cargo', max_length=100,
                                         blank=True)
    puesto_seccion_ocu_actual = models.CharField(u'sección', max_length=100,
                                                 blank=True)
    antiguedad_ocu_actual = models.CharField(u'antiguedad', max_length=50,
                                             blank=True)
    ingresos_monto = models.IntegerField(u'monto', blank=True, null=True)

    class Meta:
        abstract = True


class Inquilino(ParticipanteSolicitud):
    solicitud = models.OneToOneField('SolicitudGarantia', related_name='inquilino', null=True)

    class Meta:
        verbose_name = u'inquilino'
        verbose_name_plural = u'inquilinos'

    def __unicode__(self):
        return u'%s' % self.nombre_completo

    def save(self, *args, **kwargs):
        solicitud = self.solicitud
        solicitud.ultima_modificacion = datetime.datetime.now()
        solicitud.save()
        super(Inquilino, self).save(*args, **kwargs)


class Cosolicitante(ParticipanteSolicitud):
    solicitud = models.ForeignKey('SolicitudGarantia', related_name='cosolicitantes', null=True)

    class Meta:
        verbose_name = u'co-solicitante'
        verbose_name_plural = u'co-solicitantes'

    def __unicode__(self):
        return u'%s' % self.nombre_completo

    def save(self, *args, **kwargs):
        solicitud = self.solicitud
        solicitud.ultima_modificacion = datetime.datetime.now()
        solicitud.save()
        super(Cosolicitante, self).save(*args, **kwargs)


class AvisoIncumplimiento(models.Model):
    solicitud = models.ForeignKey('SolicitudGarantia', blank=True,
                                  null=True, editable=False)
    fecha = models.DateTimeField(u'fecha', auto_now_add=True)

    nro_cert_garantia = models.CharField(u'nro. Certificado de Garantía',
                                         max_length=6, blank=True)
    locador = models.CharField(u'locador', max_length=100)
    locatario = models.CharField(u'locatario', max_length=100)
    monto = models.IntegerField(u'monto ($)')
    fecha_vencido = models.DateField(u'vencidos el dia')

    def save(self, *args, **kwargs):
        if not self.solicitud:
            try:
                self.solicitud = SolicitudGarantia.objects.get(nro=self.nro_cert_garantia)
            except SolicitudGarantia.DoesNotExist:
                self.solicitud = None
        else:
            self.nro_cert_garantia = self.solicitud.nro
        solicitud = self.solicitud
        solicitud.ultima_modificacion = datetime.datetime.now()
        solicitud.save()
        super(AvisoIncumplimiento, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u'aviso de incumplimiento'
        verbose_name_plural = u'avisos de incumplimiento'

    def __unicode__(self):
        return u'Aviso de Incumplimiento %s' % self.pk


class EmpleadoDeInmobiliaria(models.Model):
    nombre = models.CharField(u'nombre', max_length=100)
    apellido = models.CharField(u'apellido', max_length=100, blank=True)
    email = models.EmailField(u'email', blank=True)
    nro_interno = models.CharField(u'nro de interno', max_length=30, blank=True)
    inmobiliaria = models.ForeignKey('Inmobiliaria', related_name='empleados')

    def __unicode__(self):
        return u'%s, %s' % (self.nombre, self.apellido)

    class Meta:
        verbose_name = u'empleado de inmobiliaria'
        verbose_name_plural = u'empleados de inmobiliaria'


class Inmobiliaria(models.Model):

    APROBACION_ESTADOS = (
        ('APROBADA', 'Aprobada'),
        ('NOPUBLICADA', 'No Publicada'),
        ('PENDIENTE', 'Pendiente'),
        ('RECHAZADA', 'Rechazada'),
    )
    ORIGENES = (
        ('SOLICITUD_DE_ADHESION', 'Solicitud de Adhesion'),
        ('SUGERIDO_POR_INQUILINO', 'Sugerido por Inquilino'),
        ('VENDEDOR_FINAER', 'Vendedor Finaer'),
        ('DESCONOCIDO', 'Desconocido'),
    )


    fecha_alta = models.DateTimeField(u'fecha de alta', auto_now_add=True)
    nombre = models.CharField(u'nombre', max_length=100)
    logo = ImageField(u'logo', upload_to='uploads/logos_inmobiliarias')
    domicilio = models.CharField(u'domicilio', max_length=100)
    localidades = models.ManyToManyField('Localidad',
                                         related_name="inmobiliarias", null=True)
    telefonos = models.CharField(u'telefonos', max_length=100)
    fax = models.CharField(u'fax', max_length=50, blank=True)
    email = models.EmailField(u'email')
    sitio_web = models.URLField(u'sitio web', blank=True)
    responsable = models.CharField(u'responsable', max_length=50)
    ubicacion = models.ForeignKey('Localidad', null=True, on_delete=models.PROTECT)
    observaciones = models.TextField(u'observaciones', blank=True, null=True)
    origen = models.CharField(u'origen', max_length=50,
                              choices=ORIGENES, editable=False)
    aprobacion_estado = models.CharField(u'estado de aprobacion', max_length=20,
                                         choices=APROBACION_ESTADOS,
                                         default='PENDIENTE')
    generado_por = models.ForeignKey(User, blank=True, null=True,
                                     verbose_name=u'a cargo de', on_delete=models.PROTECT)

    class Meta:
        verbose_name = u'inmobiliaria'
        verbose_name_plural = u'inmobiliarias'
        ordering = ['nombre']

    def delete(self, using=None):
        #FIXME: Use 'on_delete' instead option when upgrading to Django 1.3!
        for solicitud_relacionada in self.solicitudes.all():
            solicitud_relacionada.inmobiliaria = None
            solicitud_relacionada.save()
        super(Inmobiliaria, self).delete(using)

    def get_cantidad_solicitudes(self):
        return self.solicitudes.count()

    def get_cantidad_operaciones(self):
        return Operacion.objects.filter(solicitud__inmobiliaria__pk=self.id).count()

    def __unicode__(self):
        return u'%s' % self.nombre


class Localidad(models.Model):
    provincia_region = models.CharField(u'provincia / departamento', max_length=100,
                                        choices=PROVINCIAS_REGIONES_CHOICES)
    nombre = models.CharField(u'nombre de la localidad', max_length=100)

    class Meta:
        verbose_name = u'localidad'
        verbose_name_plural = u'localidades'
        ordering = ['nombre']

    def __unicode__(self):
        return '%s, %s' % (self.nombre, self.get_provincia_region_display())


class Consulta(models.Model):
    TIPOS_ALQUILER = (
        ('VIVIENDA', u'Vivienda'),
        ('COMERCIO', u'Comercio'),
    )

    fecha = models.DateTimeField(auto_now_add=True)
    nombre_completo = models.CharField(u'nombre y apellido', max_length=100)
    email = models.EmailField(u'email')
    telefono = models.CharField(u'teléfono', max_length=50)
    consulta = models.TextField(u'consulta')

    class Meta:
        verbose_name = u'consulta'
        verbose_name_plural = u'consultas'

    def __unicode__(self):
        return u'%s' % self.nombre_completo


class Comprobante(models.Model):
    solicitud = models.ForeignKey('SolicitudGarantia', blank=True,
                                  null=True, editable=False)
    fecha = models.DateTimeField(u'fecha', auto_now_add=True)

    descripcion = models.CharField(u'Descripción',
                                   max_length=250, blank=True)
    comprobante = models.FileField(u'comprobante', upload_to='uploads/comprobantes')

    class Meta:
        verbose_name = u'comprobante de solicitud'
        verbose_name_plural = u'comprobantes de solicitud'

    def __unicode__(self):
        return u'Comprobante %s' % self.pk

    def save(self, *args, **kwargs):
        solicitud = self.solicitud
        solicitud.ultima_modificacion = datetime.datetime.now()
        solicitud.save()
        super(Comprobante, self).save(*args, **kwargs)


class Operacion(models.Model):
    codigo = models.IntegerField(u'código', blank=True, null=True)
    solicitud = models.OneToOneField('SolicitudGarantia', verbose_name=u'solicitud',
                                     related_name='operacion', blank=True, null=True)
    anticipo = models.IntegerField(u'Anticipo', blank=True, null=True)
    cantidad_cuotas = models.IntegerField(u'Cantidad de cuotas', blank=True, null=True)
    importe_cuota = models.IntegerField(u'Importe de la cuota', blank=True, null=True)
    firmada = models.BooleanField(u'firmada?', blank=False, default=False)
    fecha_firmada = models.DateField(u'fecha firmada', blank=True, null=True)

    def save(self, *args, **kwargs):
        firmada_old = Operacion.objects.get(id=self.pk).firmada
        if not firmada_old and self.firmada:
            self.fecha_firmada = datetime.date.today()
        super(Operacion, self).save(*args, **kwargs)
        if not self.codigo:
            self.codigo = self.id + 1000
            super(Operacion, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u'operación'
        verbose_name_plural = u'operaciones'

    def __unicode__(self):
        return u'Operación %s' % self.codigo

    def get_solicitud_id(self):
        if self.solicitud.id:
            return self.solicitud.id
        else:
            return u'No asignado'

    def get_solicitud_nro(self):
        if self.solicitud.nro:
            return self.solicitud.nro
        else:
            return u'No asignado'

    def get_inquilino_nombre_completo(self):
        try:
            inquilino = self.solicitud.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if inquilino.nombre_completo:
            return u'%s' % inquilino.nombre_completo
        else:
            return u'Sin nombre'

    def get_inquilino_num_doc(self):
        try:
            inquilino = self.solicitud.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if inquilino.num_doc:
            return u'%s' % inquilino.num_doc
        else:
            return u'Sin documento'

    def get_inquilino_telefono(self):
        try:
            inquilino = self.solicitud.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if inquilino.telefono:
            return u'%s' % inquilino.telefono
        else:
            return u'Sin teléfono'

    def get_inquilino_celular(self):
        try:
            inquilino = self.solicitud.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if inquilino.celular:
            return u'%s' % inquilino.celular
        else:
            return u'Sin Celular'

    def get_inquilino_email(self):
        try:
            inquilino = self.solicitud.inquilino
        except Inquilino.DoesNotExist:
            return u'Sin inquilino'
        if inquilino.email:
            return u'%s' % inquilino.email
        else:
            return u'Sin E-mail'

    def get_firmada(self):
        if self.firmada is None:
            return 'Desconocido'
        if self.firmada:
            return 'SI'
        return 'NO'

    def get_honorario_en_contrato(self):
        return 0.33*self.get_honorario()

    def get_honorario(self):
        return self.get_importe_cuota()*self.get_cantidad_cuotas()+self.get_anticipo()

    def get_plan(self):
        cantidad_anticipo = self.get_cantidad_cuotas()
        return self.get_cantidad_cuotas()+cantidad_anticipo

    def get_importe_cuota(self):
        return float(self.importe_cuota or 0)

    def get_cantidad_cuotas(self):
        return int(self.cantidad_cuotas or 0)

    def get_anticipo(self):
        return int(self.anticipo or 0)

    def get_inmobiliaria(self):
        return self.solicitud.get_inmobiliaria()

    def get_fecha_firmada(self):
        if self.fecha_firmada is None:
            return 'No Firmada'
        else:
            return django_formats.date_format(self.fecha_firmada, "SHORT_DATE_FORMAT")


class Oficina(models.Model):
    nombre = models.CharField(u'nombre', max_length=100)
    logo = ImageField(u'logo', upload_to='uploads/oficina')
    domicilio = models.CharField(u'domicilio', max_length=100)
    ubicacion = models.ForeignKey('Localidad', null=True, on_delete=models.PROTECT)
    telefonos = models.CharField(u'telefonos', max_length=100, blank=True)
    email = models.EmailField(u'email', help_text=u'Este email será utilizado por la oficina para la recepción de las notificaciones')
    localidades = models.ManyToManyField('Localidad', related_name="oficinas", null=True, blank=True,
                                         verbose_name=u'localidades asignadas')
    publicada = models.BooleanField(u'publicada?', default=False)


    class Meta:
        verbose_name = u'oficina'
        verbose_name_plural = u'oficinas'
        ordering = ['nombre']

    def __unicode__(self):
        return u'%s' % self.nombre


class VendedorFinaer(models.Model):
    usuario = models.OneToOneField(User, related_name='vendedor', help_text=u'Sólo podrán asignarse los usuarios que pertezcan al grupo de %s' % PERMISSION_GROUPS['VENDEDOR_DE_SOLICITUDES'])
    oficina = models.ForeignKey(Oficina, related_name='vendedores')

    def __unicode__(self):
        return u'%s' % self.usuario

    class Meta:
        verbose_name = u'vendedor de Finaer'
        verbose_name_plural = u'vendedores de Finaer'


class InquilinoEncuesta(models.Model):
    COMO_NOS_CONOCIO = (
        ('INMOBILIARIA', u'Inmobiliaria'),
        ('GOOGLE', u'Google'),
        ('FACEBOOK', u'Facebook'),
        ('LINKEDIN', u'Linkedin'),
        ('AMIGO', u'Un Amigo'),
        ('MERCADOLIBRE', u'Mercado Libre'),
        ('AVISO', u'Un aviso'),
        ('OTROS', u'Otros'),
    )

    inquilino = models.OneToOneField('Inquilino', related_name='encuesta_inquilino')
    como_nos_conocio = models.CharField(u'como nos conocio', max_length=20,
                                        choices=COMO_NOS_CONOCIO,
                                        default='INMOBILIARIA')
