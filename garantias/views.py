#coding=UTF-8
from django.conf import settings
from django.contrib import messages
from django.contrib.sites.models import RequestSite
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from django.http import HttpResponse
from django.utils import simplejson
from garantias.models import *
from garantias.forms import *
from garantias.email_send import send_consulta, send_solicitud_garantia_inquilino, send_solicitud_garantia_oficina, \
    send_inmobiliaria_casa_central, send_modificacion_de_solicitud_garantia_oficina, \
    send_modificacion_de_solicitud_garantia_inquilino
from utils.paginador import paginador

def is_a_valid_document(tipo_doc, num_doc):
    inquilinos = Inquilino.objects.all()
    if tipo_doc:
        inquilinos = inquilinos.filter(tipo_doc=tipo_doc)
        if num_doc:
            inquilinos = inquilinos.filter(num_doc=num_doc)
            return not inquilinos.exists()
    return True

def solicitud_garantia_1(request):
    if request.method == 'POST':
        solicitudForm = SolicitudGarantiaForm(request.POST, prefix='solicitud')
        inquilinoForm = InquilinoForm(request.POST, prefix='inquilino')
        inquilinoEncuestaForm = InquilinoEncuestaForm(request.POST, prefix='inquilinoEncuesta')
        alquilerForm = AlquilerForm(request.POST, prefix='alquiler')
        repeatedDocumentChecked = solicitudForm.data.get('repeatedDocumentChecked')
        if inquilinoForm.is_valid() and solicitudForm.is_valid() and inquilinoEncuestaForm.is_valid() and alquilerForm.is_valid():
            tipo_docField = solicitudForm.data.get('inquilino-tipo_doc')
            num_docField = solicitudForm.data.get('inquilino-num_doc')
            if repeatedDocumentChecked == 'True' or is_a_valid_document(tipo_docField, num_docField):
                solicitudGarantia = solicitudForm.save(commit=False)
                inmobiliariaValue = solicitudForm.data['solicitud-inmobiliaria']
                if inmobiliariaValue:
                    inmobiliarias_existentes = Inmobiliaria.objects.filter(nombre=inmobiliariaValue)
                    if inmobiliarias_existentes:
                        inmobiliaria = inmobiliarias_existentes[0]
                    else:
                        inmobiliaria = Inmobiliaria.objects.create(nombre=inmobiliariaValue, origen='SUGERIDO_POR_INQUILINO')
                    solicitudGarantia.inmobiliaria = inmobiliaria
                    site = RequestSite(request)
                    send_inmobiliaria_casa_central(inmobiliaria, site)
                oficina = Oficina.objects.filter(localidades__nombre=alquilerForm.cleaned_data['localidad_barrio'])
                if oficina.exists():
                    solicitudGarantia.oficina = oficina[0]
                else:
                    solicitudGarantia.oficina = Oficina.objects.get(pk=settings.CENTRAL_OFFICE_ID)
                solicitudGarantia.save()
                inquilino = inquilinoForm.save(commit=False)
                inquilino.solicitud = solicitudGarantia
                inquilino.save()
                inquilinoEncuesta = inquilinoEncuestaForm.save(commit=False)
                inquilinoEncuesta.inquilino = inquilino
                inquilinoEncuesta.save()
                messages.success(request, u'Ha completado el paso 1 correctamente.')
                return redirect('solicitud_garantia_2', nro_solicitud=solicitudGarantia.nro)
            else:
                repeatedDocumentChecked = True
                messages.add_message(request, messages.SUCCESS, u"Ya se encuentra ese documento cargado en el sistema, si está seguro \
                                              seleccione nuevamente la opción de continuar. Para agregar información a la solicitud  \
                                              prexistente por favor ingrese <a href='%s'>aquí</a>" % '/solicitud-garantia/agregar_informacion')
    else:
        solicitudForm = SolicitudGarantiaForm(prefix='solicitud')
        inquilinoForm = InquilinoForm(prefix='inquilino')
        inquilinoEncuestaForm = InquilinoEncuestaForm(prefix='inquilinoEncuesta')
        alquilerForm = AlquilerForm(prefix='alquiler')
        repeatedDocumentChecked = False
    return render_to_response('solicitud_garantia/solicitud_garantia.html',
                              {'solicitudForm': solicitudForm,
                               'inquilinoForm': inquilinoForm,
                               'inquilinoEncuestaForm': inquilinoEncuestaForm,
                               'alquilerForm': alquilerForm,
                               'repeatedDocumentChecked': repeatedDocumentChecked},
                              context_instance=RequestContext(request))


def solicitud_garantia_2(request, nro_solicitud):
    solicitud = get_object_or_404(SolicitudGarantia, nro=nro_solicitud)
    oficina = solicitud.oficina
    oficinaForm = OficinaForm(prefix='oficina', initial={'nombre': oficina.nombre,
                                                         'domicilio': oficina.domicilio,
                                                         'ubicacion': oficina.ubicacion,
                                                         'telefonos': oficina.telefonos,
                                                         'email': oficina.email})
    if request.method == 'POST':
        cosolicitanteForm = CosolicitanteForm(request.POST)
        if cosolicitanteForm.is_valid():
            cosolicitante = cosolicitanteForm.save(commit=False)
            cosolicitante.solicitud = solicitud
            cosolicitante.save()
            return redirect('solicitud_garantia_3', nro_solicitud=nro_solicitud)
    else:
        cosolicitanteForm = CosolicitanteForm()
    return render_to_response('solicitud_garantia/solicitud_garantia_paso_2.html',
                              {'cosolicitanteForm': cosolicitanteForm,
                               'nro_solicitud': nro_solicitud,
                               'oficinaForm': oficinaForm},
                              context_instance=RequestContext(request))


def solicitud_garantia_3(request, nro_solicitud):
    solicitud = get_object_or_404(SolicitudGarantia, nro=nro_solicitud)
    if request.method == 'POST':
        comprobanteForm = ComprobanteForm(request.POST, request.FILES)
        if comprobanteForm.is_valid():
            comprobante = comprobanteForm.save(commit=False)
            comprobante.solicitud = solicitud
            comprobante.save()
            #Envio de emails
            site = RequestSite(request)
            send_solicitud_garantia_oficina(solicitud, site, solicitud.oficina)
            send_solicitud_garantia_inquilino(solicitud)
            return render_to_response('solicitud_garantia/solicitud_garantia_enviada.html',
                                      {'nro_solicitud': nro_solicitud,},
                                      context_instance=RequestContext(request))
    else:
        comprobanteForm = ComprobanteForm()
    return render_to_response('solicitud_garantia/solicitud_garantia_paso_3.html',
                              {'comprobanteForm': comprobanteForm,
                               'nro_solicitud': nro_solicitud},
                              context_instance=RequestContext(request))


def agregar_info_solicitud(request):
    showOficina = False
    oficinaForm = OficinaForm()
    if request.method == 'POST':
        nroSolicitudForm = NroSolicitudForm(request.POST)
        if nroSolicitudForm.is_valid():
            nro = nroSolicitudForm.cleaned_data['nro_solicitud']
            solicitud = SolicitudGarantia.objects.get(nro=nro)
            oficina = solicitud.oficina
            oficinaForm = OficinaForm(prefix='oficina', initial={'nombre': oficina.nombre,
                                                                 'domicilio': oficina.domicilio,
                                                                 'ubicacion': oficina.ubicacion,
                                                                 'telefonos': oficina.telefonos,
                                                                 'email': oficina.email})
            if not solicitud.estado in ['ACTIVA', 'INCOMPLETA', 'ENPROCESO']:
                messages.error(request, 'Estamos procesando esta solicitud, por lo tanto no puede ser modificada. Ante '
                                        'cualquier duda comuniquese con nosotros.')
                showOficina = True
            else:
                if request.POST['selectedOption'] == 'cosolicitante':
                    return redirect('nuevo_cosolicitante', nro_solicitud=nro)
                if request.POST['selectedOption'] == 'comprobante':
                    return redirect('nuevo_comprobante', nro_solicitud=nro)
                if request.POST['selectedOption'] == 'modificar-solicitud':
                    return redirect('modificar_solicitud_garantia', nro_solicitud=nro)
    else:
        nroSolicitudForm = NroSolicitudForm()
    return render_to_response('solicitud_garantia/agregar_informacion.html',
                              {'nroSolicitudForm': nroSolicitudForm,
                               'showOficina': showOficina,
                               'oficinaForm': oficinaForm},
                              context_instance=RequestContext(request))


def nuevo_cosolicitante(request, nro_solicitud):
    solicitud = get_object_or_404(SolicitudGarantia, nro=nro_solicitud, estado__in=['ACTIVA', 'INCOMPLETA', 'ENPROCESO'])
    if request.method == 'POST':
        cosolicitanteForm = CosolicitanteForm(request.POST)
        if cosolicitanteForm.is_valid():
            cosolicitante = cosolicitanteForm.save(commit=False)
            cosolicitante.solicitud = solicitud
            cosolicitante.save()
            cosolicitanteForm = CosolicitanteForm()
            #Envio de emails
            site = RequestSite(request)
            send_modificacion_de_solicitud_garantia_oficina(solicitud, site, solicitud.oficina)
            send_modificacion_de_solicitud_garantia_inquilino(solicitud)
            messages.success(request, 'El co-solicitante se ha agregado exitosamente. Puede agregar otro si lo desea.')
    else:
        cosolicitanteForm = CosolicitanteForm()
    return render_to_response('solicitud_garantia/nuevo_cosolicitante.html',
                              {'cosolicitanteForm': cosolicitanteForm,
                               'nro_solicitud': nro_solicitud},
                              context_instance=RequestContext(request))


def modificar_solicitud_garantia(request, nro_solicitud):
    solicitud = get_object_or_404(SolicitudGarantia, nro=nro_solicitud, estado__in=['ACTIVA', 'INCOMPLETA', 'ENPROCESO'])
    oficina = solicitud.oficina
    oficinaForm = OficinaForm(prefix='oficina', initial={'nombre': oficina.nombre,
                                                         'domicilio': oficina.domicilio,
                                                         'ubicacion': oficina.ubicacion,
                                                         'telefonos': oficina.telefonos,
                                                         'email': oficina.email})
    if request.method == 'POST':
        solicitudForm = ModificacionDeSolicitudGarantiaForm(request.POST, instance=solicitud, prefix='solicitud')
        inquilinoForm = ModificacionDeInquilinoForm(request.POST, instance=solicitud.inquilino, prefix='inquilino')
        if inquilinoForm.is_valid() and solicitudForm.is_valid():
            solicitudForm.save()
            inquilinoForm.save()
            #Envio de emails
            site = RequestSite(request)
            send_modificacion_de_solicitud_garantia_oficina(solicitud, site, solicitud.oficina)
            send_modificacion_de_solicitud_garantia_inquilino(solicitud)
            return render_to_response('solicitud_garantia/modificacion_de_solicitud_garantia_enviada.html',
                                      {'nro_solicitud': nro_solicitud,},
                                      context_instance=RequestContext(request))
    else:
        solicitudForm = ModificacionDeSolicitudGarantiaForm(instance=solicitud, prefix='solicitud')
        inquilinoForm = InquilinoForm(instance=solicitud.inquilino, prefix='inquilino')
    return render_to_response('solicitud_garantia/modificar_solicitud_garantia.html',
                              {'solicitudForm': solicitudForm,
                               'inquilinoForm': inquilinoForm,
                               'oficinaForm': oficinaForm},
                              context_instance=RequestContext(request))





def nuevo_comprobante(request, nro_solicitud):
    solicitud = get_object_or_404(SolicitudGarantia, nro=nro_solicitud, estado__in=['ACTIVA', 'INCOMPLETA', 'ENPROCESO'])
    if request.method == 'POST':
        comprobanteForm = ComprobanteForm(request.POST, request.FILES)
        if comprobanteForm.is_valid():
            comprobante = comprobanteForm.save(commit=False)
            comprobante.solicitud = solicitud
            comprobante.save()
            comprobanteForm = ComprobanteForm()
            #Envio de emails
            site = RequestSite(request)
            send_modificacion_de_solicitud_garantia_oficina(solicitud, site, solicitud.oficina)
            send_modificacion_de_solicitud_garantia_inquilino(solicitud)
            messages.success(request, 'El comprobante se ha agregado exitosamente. Puede agregar otro si lo desea.')
    else:
        comprobanteForm = ComprobanteForm()
    return render_to_response('solicitud_garantia/nuevo_comprobante.html',
                              {'comprobanteForm': comprobanteForm,
                               'nro_solicitud': nro_solicitud},
                              context_instance=RequestContext(request))


def aviso_incumplimiento(request):
    if request.method == 'POST':
        avisoForm = AvisoIncumplimientoForm(request.POST)
        if avisoForm.is_valid():
            avisoForm.save()
            return render_to_response('propietarios/aviso_incumplimiento_enviado.html',
                                      context_instance=RequestContext(request))
    else:
        avisoForm = AvisoIncumplimientoForm()
    return render_to_response('propietarios/aviso_incumplimiento.html',
                              {'avisoForm': avisoForm, },
                              context_instance=RequestContext(request))


def inmobiliarias_adhesion(request):
    if request.method == 'POST':
        inmobiliaria_con_origen = Inmobiliaria(origen='SOLICITUD_DE_ADHESION')
        adhesionForm = InmobiliariaAdhesionForm(request.POST, request.FILES, instance=inmobiliaria_con_origen)
        if adhesionForm.is_valid():
            inmobiliaria = adhesionForm.save()
            site = RequestSite(request)
            send_inmobiliaria_casa_central(inmobiliaria, site)
            return render_to_response('inmobiliarias/adhesion_enviada.html',
                                      context_instance=RequestContext(request))
    else:
        adhesionForm = InmobiliariaAdhesionForm()
    return render_to_response('inmobiliarias/adhesion.html',
                              {'adhesionForm': adhesionForm, },
                              context_instance=RequestContext(request))


def inmobiliarias_adheridas(request):
    has_filters = False
    inmobiliarias = Inmobiliaria.objects.filter(aprobacion_estado='APROBADA')
    if 'buscar' in request.GET:
        has_filters = True
        buscadorForm = BuscadorInmobiliariasForm(request.GET)
        if 'provincia_region' in request.GET and request.GET['provincia_region'] != 'ALL':
            inmobiliarias = inmobiliarias.filter(
                localidades__provincia_region=request.GET['provincia_region']).distinct()
        if 'localidad_barrio' in request.GET and request.GET['localidad_barrio'] != 'Todos':
            inmobiliarias = inmobiliarias.filter(localidades__nombre=request.GET['localidad_barrio']).distinct()
        if 'nombre' in request.GET:
            inmobiliarias = inmobiliarias.filter(nombre__icontains=request.GET['nombre']).distinct()
    else:
        buscadorForm = BuscadorInmobiliariasForm()
    pag = paginador(request, inmobiliarias, settings.INMOBILIARIAS_PER_PAGE)
    return render_to_response('inmobiliarias/inmobiliarias_adheridas.html',
                              {'paginator': pag,
                               'has_filters': has_filters,
                               'buscadorForm': buscadorForm, },
                              context_instance=RequestContext(request))


#Busco por AJAX las localidades para el filtro de inmobiliarias adheridas
def ajax_get_localidades(request):
    localidades = []
    if 'q' in request.GET:
        localidades = Localidad.objects.filter(provincia_region=request.GET['q']).values('nombre')
    localidades = ['Todos', ] + list(localidades)
    return HttpResponse(simplejson.dumps(localidades),
                        mimetype='application/json')

#Busco por AJAX las localidades para el filtro de inmobiliarias adheridas
def ajax_get_localidades_solicitud(request):
    localidades = []
    if 'q' in request.GET:
        localidades = Localidad.objects.filter(provincia_region=request.GET['q']).values('nombre')
    localidades = ['Seleccione...', ] + list(localidades)
    return HttpResponse(simplejson.dumps(localidades),
                        mimetype='application/json')


def ajax_get_inmobiliarias(request):
    if request.method == 'GET':
        if 'q' in request.GET:
            searchValue = request.GET['q']
            inmobiliarias = Inmobiliaria.objects.filter(nombre__icontains=searchValue)
        else:
            inmobiliarias = Inmobiliaria.objects.all()
        inmobiliarias = [{'nombre': inmobiliaria.nombre} for inmobiliaria in inmobiliarias]
        return HttpResponse(simplejson.dumps(inmobiliarias), mimetype='application/json')


def consultas(request):
    if request.method == 'POST':
        consultaForm = ConsultaForm(request.POST)
        if consultaForm.is_valid():
            nuevaConsulta = consultaForm.save()
            envio_ok = send_consulta(nuevaConsulta)
            if envio_ok:
                return render_to_response('consulta_enviada.html',
                                          {'hasErrors': False},
                                          context_instance=RequestContext(request))
        return render_to_response('consulta_enviada.html',
                                  {'hasErrors': True},
                                  context_instance=RequestContext(request))
    else:
        consultaForm = ConsultaForm()
    return render_to_response('contacto.html',
                              {'consultaForm': consultaForm},
                              context_instance=RequestContext(request))


def oficinas(request):
    oficina = Oficina.objects.all().order_by('nombre')
    pag = paginador(request, oficina, 3)
    return render_to_response('representantes.html',
                              {'paginator': pag},
                              context_instance=RequestContext(request))